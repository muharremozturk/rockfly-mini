EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 15
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L roket-rescue:ESP32-WROOM-32D-ESP32-WROOM-32D U11
U 1 1 60932951
P 6200 3700
F 0 "U11" H 6200 5067 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 6200 4976 50  0000 C CNN
F 2 "others:ESP32-WROOM-32D" H 6200 3700 50  0001 L BNN
F 3 "" H 6200 3700 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 6200 3700 50  0001 L BNN "STANDARD"
F 5 "3.2 mm" H 6200 3700 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "Espressif Systems" H 6200 3700 50  0001 L BNN "MANUFACTURER"
F 7 "1.9" H 6200 3700 50  0001 L BNN "PARTREV"
	1    6200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR077
U 1 1 6093CB48
P 8100 2450
F 0 "#PWR077" H 8100 2300 50  0001 C CNN
F 1 "+3.3V" H 8115 2623 50  0000 C CNN
F 2 "" H 8100 2450 50  0001 C CNN
F 3 "" H 8100 2450 50  0001 C CNN
	1    8100 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR076
U 1 1 6093D8D8
P 4400 2300
F 0 "#PWR076" H 4400 2150 50  0001 C CNN
F 1 "+3.3V" H 4415 2473 50  0000 C CNN
F 2 "" H 4400 2300 50  0001 C CNN
F 3 "" H 4400 2300 50  0001 C CNN
	1    4400 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR081
U 1 1 6093DFCE
P 7150 4900
F 0 "#PWR081" H 7150 4650 50  0001 C CNN
F 1 "GND" H 7155 4727 50  0000 C CNN
F 2 "" H 7150 4900 50  0001 C CNN
F 3 "" H 7150 4900 50  0001 C CNN
	1    7150 4900
	1    0    0    -1  
$EndComp
$Comp
L roket-rescue:Cap-805_cap C22
U 1 1 6093F1B1
P 7700 2700
F 0 "C22" V 7704 2803 50  0000 L CNN
F 1 "22uF" V 7795 2803 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7700 2700 50  0001 L BNN
F 3 "" H 7700 2700 50  0001 L BNN
	1    7700 2700
	0    1    1    0   
$EndComp
$Comp
L roket-rescue:Cap-805_cap C23
U 1 1 6093FD89
P 8100 2700
F 0 "C23" V 8104 2803 50  0000 L CNN
F 1 "0.1uF" V 8195 2803 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8100 2700 50  0001 L BNN
F 3 "" H 8100 2700 50  0001 L BNN
	1    8100 2700
	0    1    1    0   
$EndComp
$Comp
L roket-rescue:Cap-805_cap C24
U 1 1 609405B2
P 4400 3000
F 0 "C24" V 4404 3103 50  0000 L CNN
F 1 "0.1uF" V 4495 3103 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4400 3000 50  0001 L BNN
F 3 "" H 4400 3000 50  0001 L BNN
	1    4400 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R26
U 1 1 60941843
P 4400 2550
F 0 "R26" H 4468 2596 50  0000 L CNN
F 1 "10K" H 4468 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4440 2540 50  0001 C CNN
F 3 "~" H 4400 2550 50  0001 C CNN
	1    4400 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR080
U 1 1 60942F63
P 4400 3300
F 0 "#PWR080" H 4400 3050 50  0001 C CNN
F 1 "GND" H 4405 3127 50  0000 C CNN
F 2 "" H 4400 3300 50  0001 C CNN
F 3 "" H 4400 3300 50  0001 C CNN
	1    4400 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR078
U 1 1 60946688
P 8100 3050
F 0 "#PWR078" H 8100 2800 50  0001 C CNN
F 1 "GND" H 8105 2877 50  0000 C CNN
F 2 "" H 8100 3050 50  0001 C CNN
F 3 "" H 8100 3050 50  0001 C CNN
	1    8100 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2450 8100 2600
Wire Wire Line
	7000 2600 7700 2600
Connection ~ 8100 2600
Connection ~ 7700 2600
Wire Wire Line
	7700 2600 8100 2600
Wire Wire Line
	7700 2900 7700 2950
Wire Wire Line
	7700 2950 8100 2950
Wire Wire Line
	8100 2950 8100 2900
Wire Wire Line
	8100 3050 8100 2950
Connection ~ 8100 2950
Wire Wire Line
	7000 4850 7150 4850
Wire Wire Line
	7150 4850 7150 4900
Wire Wire Line
	7150 4700 7000 4700
Wire Wire Line
	7150 4600 7000 4600
Wire Wire Line
	7150 4500 7000 4500
Wire Wire Line
	7150 4400 7000 4400
Wire Wire Line
	7150 4300 7000 4300
Wire Wire Line
	7150 4200 7000 4200
Wire Wire Line
	7150 4100 7000 4100
Wire Wire Line
	7150 4000 7000 4000
Wire Wire Line
	7150 3900 7000 3900
Wire Wire Line
	7150 3800 7000 3800
Wire Wire Line
	7150 3700 7000 3700
Wire Wire Line
	7150 2800 7000 2800
Wire Wire Line
	7150 2900 7000 2900
Wire Wire Line
	7150 3000 7000 3000
Wire Wire Line
	7150 3100 7000 3100
Wire Wire Line
	7150 3200 7000 3200
Wire Wire Line
	7150 3300 7000 3300
Wire Wire Line
	7150 3400 7000 3400
Wire Wire Line
	7150 3500 7000 3500
Wire Wire Line
	7150 3600 7000 3600
Wire Wire Line
	5400 3100 5300 3100
Wire Wire Line
	5400 3200 5300 3200
Wire Wire Line
	5400 3400 5300 3400
Wire Wire Line
	5400 3500 5300 3500
Wire Wire Line
	5400 3700 5300 3700
Wire Wire Line
	5400 3800 5300 3800
Wire Wire Line
	4400 3300 4400 3200
Wire Wire Line
	4400 2700 4400 2750
Connection ~ 4400 2800
Wire Wire Line
	4400 2800 4400 2900
Wire Wire Line
	4400 2750 4300 2750
Connection ~ 4400 2750
Wire Wire Line
	4400 2750 4400 2800
Wire Wire Line
	4400 2400 4400 2300
$Comp
L Device:R_US R27
U 1 1 60957214
P 8850 3500
F 0 "R27" H 8918 3546 50  0000 L CNN
F 1 "10K" H 8918 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8890 3490 50  0001 C CNN
F 3 "~" H 8850 3500 50  0001 C CNN
	1    8850 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R28
U 1 1 60957FEA
P 9200 3500
F 0 "R28" H 9268 3546 50  0000 L CNN
F 1 "10K" H 9268 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9240 3490 50  0001 C CNN
F 3 "~" H 9200 3500 50  0001 C CNN
	1    9200 3500
	1    0    0    -1  
$EndComp
Text Label 8600 3700 0    50   ~ 0
SCL
Text Label 8600 3850 0    50   ~ 0
SDA
Text Label 7000 4100 0    50   ~ 0
SCL
Text Label 7000 4000 0    50   ~ 0
SDA
$Comp
L power:+3.3V #PWR079
U 1 1 6095D268
P 9050 3200
F 0 "#PWR079" H 9050 3050 50  0001 C CNN
F 1 "+3.3V" H 9065 3373 50  0000 C CNN
F 2 "" H 9050 3200 50  0001 C CNN
F 3 "" H 9050 3200 50  0001 C CNN
	1    9050 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 3200 9050 3350
Wire Wire Line
	9050 3350 8850 3350
Wire Wire Line
	9050 3350 9200 3350
Connection ~ 9050 3350
Wire Wire Line
	8850 3650 8850 3700
Wire Wire Line
	8850 3700 8600 3700
Wire Wire Line
	8600 3850 9200 3850
Wire Wire Line
	9200 3850 9200 3650
Text HLabel 4300 2750 0    50   Input ~ 0
EN
Text HLabel 5300 3100 0    50   Input ~ 0
SENSOR_VP
Wire Wire Line
	4400 2800 5400 2800
Text HLabel 5300 3200 0    50   Input ~ 0
SENSOR_VN
Text HLabel 5300 3400 0    50   Input ~ 0
IO34
Text HLabel 5300 3500 0    50   Input ~ 0
IO35
Text HLabel 5300 3700 0    50   Input ~ 0
TXD0
Text HLabel 5300 3800 0    50   Input ~ 0
RXD0
Text HLabel 7150 2800 2    50   Input ~ 0
IO0
Text HLabel 7150 2900 2    50   Input ~ 0
IO2
Text HLabel 7150 3000 2    50   Input ~ 0
IO4
Text HLabel 7150 3100 2    50   Input ~ 0
IO5
Text HLabel 7150 3200 2    50   Input ~ 0
IO12
Text HLabel 7150 3300 2    50   Input ~ 0
IO13
Text HLabel 7150 3400 2    50   Input ~ 0
IO14
Text HLabel 7150 3500 2    50   Input ~ 0
IO15
Text HLabel 7150 3600 2    50   Input ~ 0
IO16
Text HLabel 7150 3700 2    50   Input ~ 0
IO17
Text HLabel 7150 3800 2    50   Input ~ 0
IO18
Text HLabel 7150 3900 2    50   Input ~ 0
IO19
Text HLabel 7150 4000 2    50   Input ~ 0
IO21
Text HLabel 7150 4100 2    50   Input ~ 0
IO22
Text HLabel 7150 4200 2    50   Input ~ 0
IO23
Text HLabel 7150 4300 2    50   Input ~ 0
IO25
Text HLabel 7150 4400 2    50   Input ~ 0
IO26
Text HLabel 7150 4500 2    50   Input ~ 0
IO27
Text HLabel 7150 4600 2    50   Input ~ 0
IO32
Text HLabel 7150 4700 2    50   Input ~ 0
IO33
$EndSCHEMATC
