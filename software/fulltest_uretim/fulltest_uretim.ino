#include "FS.h"
#include "LITTLEFS.h"
#include "ms5611_spi.h"
#include <SPI.h>

float Ms5611_data[2]={0,0};

#define buzzer 21 //buzzer pin  
#define pyro1 32 //pyro1 io expender pin
#define pyro2 16 //pyro2 io expender pin
#define pyro1S 39 //pyro1 measurement pin
#define pyro2S 15 //pyro2 measurement pin
#define led1 17 //led1 io expender pin
#define led2 25 //led2 io expender pin
#define battPin 13 //battery measurement pin
#define pushButton 0 //battery measurement pin

float battMin = 7;
float battMax = 8;
float TempMin = 27.0;
float TempMax = 35.0;
#define PressMin 88800
#define PressMax 95500

uint8_t MS_CS = 5;
baro_ms5611 MS5611(MS_CS);

void setup() {
  Serial.begin(115200);
  while (!Serial)
    ;
  Serial.println("Serial    :[OK]");
  setPins();
  configMs5611();
  readBat();
  readMs5611();
  pyroTest();

  Serial.println(F("  _______        _      ____  _  __"));
  Serial.println(F(" |__   __|      | |    / __ \\| |/ /"));
  Serial.println(F("    | | ___  ___| |_  | |  | | ' / "));
  Serial.println(F("    | |/ _ \\/ __| __| | |  | |  <  "));
  Serial.println(F("    | |  __/\\__ \\ |_  | |__| | . \\ "));
  Serial.println(F("    |_|\\___||___/\\__|  \\____/|_|\\_\\"));
}

void loop() {
}

void setPins() {
  pinMode(pyro1, OUTPUT);
  pinMode(pyro2, OUTPUT);
  pinMode(MS_CS, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  digitalWrite(pyro1, 0);
  digitalWrite(pyro2, 0);
  digitalWrite(MS_CS, 1);
  digitalWrite(buzzer, 0);
  digitalWrite(led1, 0);
  digitalWrite(led2, 0);
  pinMode(pushButton, INPUT_PULLUP);
}
void pyroTest(){
  while(!pyro1Read()){
    Serial.println("Pyro 1 takili degil!!!");
    delay(1000);
  }
  Serial.println("Pyro 1 takili   OK");
  if(!pyro2Read()){
    Serial.println("Pyro 2 takili degil!!!");
    while(1);
  }
  Serial.println("Pyro 2 takili   OK");
  delay(1000);
  Serial.println("Pyro 1 atesleme test");
  digitalWrite(pyro1, 1);
  if(pyro1Read()){
    Serial.println("Pyro 1 ateslenmedi!!!");
    while(1);
  }
  Serial.println("Pyro 1 ateşlendi     OK");
  Serial.println("Pyro 2 atesleme test");
  digitalWrite(pyro2, 1);
  while(pyro2Read()){
    Serial.println("Pyro 2 ateslenmedi!!!");
    delay(1000);
  }
  Serial.println("Pyro 2 ateşlendi     OK");
}
void configMs5611() {
  SPI.begin(18,19,23,5);
  MS5611.initialize();
  MS5611.calibrateAltitude();
}
//fire pyro1
void pyro1Fire() {
    digitalWrite(pyro1, HIGH);
}
//Off pyro1
void pyro1Off() {
  digitalWrite(pyro1, LOW);
}
//read pyro1
bool pyro1Read() {
  if ((analogRead(pyro1S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
//fire pyro2
void pyro2Fire() {
  digitalWrite(pyro2, HIGH);
}
//Off pyro2
void pyro2Off() {
  digitalWrite(pyro2, LOW);
}
//read pyro2
bool pyro2Read() {
  if ((analogRead(pyro2S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
void readBat() {
  float R1 = 560000.0;  // 560K
  float R2 = 100000.0;  // 100K
  float value = 0.0f;
  value = analogRead(battPin);
  value += analogRead(battPin);
  value += analogRead(battPin);
  value = value / 3.0;
  value = (value * 3.33) / 4096.0;
  value = value / (R2 / (R1 + R2));
  Serial.println("[OK]");
  Serial.print("Batarya Voltaji  (");
  Serial.print(value);
  Serial.print(" V):");
  if (battMax > value && value> battMin) {
    Serial.println("[OK]");
  } 
  else {
    Serial.println("Batarya Voltaji Uygun Aralikta Degil!!!");
    while (1);
  }
}

void readMs5611() {
  for (int i = 0; i < 10; i++) {
    MS5611.updateData();
    Ms5611_data[0] += MS5611.getTemperature_degC();
    Ms5611_data[1] += MS5611.getPressure_mbar();
    delay(100);
  }
  if (TempMin < (Ms5611_data[0] / 10) < TempMax && PressMin < (Ms5611_data[1] / 10) < PressMax) {
    Serial.println("MS5611 VERİ TESTİ   [OK]");
  } 
  else {
    Serial.println("MS5611 Bozuk data");
    while (1);
  }
}
