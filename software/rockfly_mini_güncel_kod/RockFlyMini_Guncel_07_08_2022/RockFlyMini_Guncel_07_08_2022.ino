#include <SPI.h> 
#include <Wire.h>
#include <WiFi.h>  
#include <WebServer.h>
#include "EEPROM.h"
#include "FS.h"  
#include "LITTLEFS.h"
#include "ms5611_spi.h"  // https://github.com/parmAshu/ms5611_driver ,but we made updates in the library.
#include <ESP32Time.h>  //https://github.com/fbiego/ESP32Time
#include <SimpleKalmanFilter.h> //https://github.com/denyssene/SimpleKalmanFilter
#include <Vector.h> // https://github.com/janelia-arduino/Vector

float mainPar = 20;  //main parachute replacement 
bool measurementSystem = 0; //change the measurement system , chose only 0 or 1 -> "0" is metric system , "1" is imperial system 
uint8_t drogueDifference =2;  //how many meters from the drogue will the parachute open

//flight and write modes must be turned on together
bool flightMode = 0; //turn on flight mode
bool writeMode = 0; //turn on write mode

const uint8_t tempData =20; //buffer size ,do not change . If the value in this field changes, you will have problems with the recording speed and data reading frequency.

SimpleKalmanFilter speedKalmanFilter(1, 1, 0.1); // speed filter
SimpleKalmanFilter accelKalmanFilter(1, 1, 0.1); // acceleration filter

ESP32Time rtc; //real time clock

//WiFi Settings
#define ServerVersion "1.0"
String webpage = "";
#define SPIFFS LITTLEFS
bool SPIFFS_present = false;
const char *ssid = "RockFLY-MINI";              //WiFi id
const char *password = "rockflymini2022";       //WiFi password

WebServer server(80);

//for pre-flight data
const int ELEMENT_COUNT_MAX = 50; //The last 50 data are kept before the flight, do not  change it.If you change this value, you may not be able to see the pre-flight data and you may have problems writing to the flash memory after the flight.
typedef Vector<String> Elements;
String storage_array[ELEMENT_COUNT_MAX];
Elements vector;


#define buzzer 21 //buzzer io pin  
#define pyro1 16 //pyro1 io pin
#define pyro2 32 //pyro2 io pin
#define pyro1S 39 //pyro1 measurment pin
#define pyro2S 36 //pyro2 measurement pin
#define led1 17 //led1 io pin
#define led2 25 //led2 io expender pin
#define batt 35 //battery measurement pin

uint8_t pyroStat; 
bool pyroStatArr[8]; //pyro state array

float accelMs5611; //ms5611 accel data
long accelMs5611TimerNew, accelMs5611TimerOld; //ms5611 accel timer
float speed;    //ms5611 speed data
float speedOld; //ms5611 pre-speed data
float altitude; //ms5611 altitude data
float fillRate; // flash memory occupancy rate
int FlightID =0;// number of flight

//pyro variables
bool pyroFire1 = 0; //first pyro's explosion info
bool pyroFire2 = 0; //second pyro's explosion info
bool pyroProt1 = 0; //first pyro's protection 
bool pyroProt2 = 0; //second pyro's protection 

bool riseAccelStat=0;  //average acceleration from ground to apooge 
bool fallAccelStat=0;  //average acceleration from apooge to ground 
float riseAccelCount = 0; //number of acceleration data read from ground to apooge
float fallAccelCount = 0; //number of acceleration data read from apooge to ground
bool apfallSpeedStart = 0; //starting point for measuring average velocity from apogee to main parachute  
unsigned int apfallSpeedCount = 0;//number of speed data read from apogee to main parachute
bool mainfallSpeedStart = 0; //starting point for measuring average velocity from main parachute to ground 
unsigned int mainfallSpeedCount = 0;//number of speed data read from main parachute to ground 
long pyroActivationTime=0;//Time from apogee to the first pyro explosion 
bool apogeeCheck=0; // apogee detection 
bool machLock = 0; // machlock detection 
bool groundMode = 0; // ground detection 
bool groundModee = 0; // ground detection , do not change this, We prevent the flight from starting again after the flight is over.


struct flightData {
  float Ms5611_data[4];        //temp, pressure, speed, accel
  float battery;               //battery voltage 
  float maxAltitude;           //max altitude
  float minAltitude;           //min altitude
  float maxSpeed;              //max speed
  float maxAccel;              //max acceleration 
  unsigned long apogeeTime;    //apogee time
  float apogeeFallSpeed;       //apogee fall speed
  float mainFallSpeed;         //main fall speed
  unsigned long descendTime;   //descend time
  unsigned long flightTime;    //flight time
  unsigned long unixTime;      //unix time 
  float AverageRiseAccel;      //average rise acceleration  
  float AverageFallAccel;      //average fall acceleration
  
};
flightData flightdata;

//Ms5611 definitions
uint8_t MS_CS = 5;
baro_ms5611 MS5611(MS_CS);

File file;

//Temporary buffer data array
String All_data1[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.
String All_data2[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.
String All_data3[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.
String All_data4[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.

//Temporary buffer data array for flash
String All_data_flash1[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.
String All_data_flash2[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.
String All_data_flash3[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.
String All_data_flash4[tempData]; //temporary buffer where data is stored for writing to flash memory, Do not change it, if you change it, your writing speed to the flash memory will change and you will lose data.

//Flash header 
String header = "Temp;Press;Alt;Speed;Accel;Batt;PyroStat;Time\n";
String headerStat = "MaxAltitude;MaxSpeed;MaxAccel;ApogeeTime;ApogeeFallSpeed;MainFallSpeed;DescendTime;PyroActivationTime(ms);AverageRiseAccel;AverageFallAccel;FlightTime\n";
String statData; // statistics data

String fileName[20];      // for flash memory, do not change 
String directoryName[20]; // for flash memory, do not change
int countFileName = 0;    // for flash memory, do not change
int countDirecName = 0;   // for flash memory, do not change

int memory = 0; //buffer memory counter
int pre_buffer_memory_counter = 0; //pre buffer memory counter

//flash check bits
bool flashCheck1 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.
bool flashCheck2 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.
bool flashCheck3 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.
bool flashCheck4 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.

//write check bits
bool writeCheck1 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.
bool writeCheck2 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.
bool writeCheck3 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.
bool writeCheck4 = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.

uint8_t dataCheck = 0; //developed for flash memory writing algorithm. Do not change. If you change it, the speed of writing to the flash memory will change and you will lose data.
bool storageArrayWrite = 0; //writing 50 pre-flight data to flash memory, do not change.
bool configFlashCheck = 0; //protection for writing flight data to flash memory
bool writeStatCheck = 0; //protection for writing statistics data to flash memory

int pushButton = 0; //wifi mode button
unsigned long wifiModeTimer = 0; //time taken to switch to wifi mode

TaskHandle_t Task1; // second core variable

unsigned long start = 0; //for read data timer
String Flight_Name; //file name where flight data is stored


void setup() {
  Serial.begin(115200);
  setPins();          //pin settings
  configFlash();      //flash settings
  configMs5611();     //ms5611 settings
  buzzerToggle(5, 500); //star buzzer
  wifiMode();         //wifi mode settings
 
  //start core 2
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */

}

// Flash recording in core 2
void Task1code(void *pvParameters) {

  for (;;) {
    vTaskDelay(1);
    if (writeMode) {
      if (!configFlashCheck) {
        appendFile(SPIFFS,  Flight_Name.c_str() , header); //write header one times
        configFlashCheck = 1;
      }
      flashWrite(); //Write data to flash memory
    }
  }
}

void loop() {
  vTaskDelay(1); //required for second core operation, do not change

  if (!flightMode == 1) {
    preFlightAlgorithm(); //Runs the pre-flight algorithm
  } else {

    if (millis() - start >= 30) {
      start = millis();
      readMs5611();       //read ms5611 
      readVoltage();      //read voltage
      flightAlgorithm();  //runs the flight algorithm
      writeData();        //collects the data in an array and sends it to the 2nd core
    }
  }
}
void preFlightAlgorithm() {
  if (millis() - start >= 90) { //to read 10Hz data before flight
    start = millis();
    if (!groundModee) {  //read data out of ground mode
      readMs5611();     //read ms5611
      readVoltage();    //read voltage
      String data;  
      addDataString(data);//add data to string

      //collect 50 data in vector
      if (vector.size() == vector.max_size()) { // If the number of data stored before the flight reaches 50, it deletes the first value it read and overwrites it one by one.
        vector.remove(0); 
      }
      vector.push_back(data);
    }
  }

  //flight detection  
  if ((altitude > 10)&&(!groundModee)) {
    
    char *name1= "/Flight";  // Creat file name in Flash Memory
    char *name2= ".csv";     // Creat file name in Flash Memory
    Flight_Name = name1 + String(FlightID) +name2; //Creat file name in Flash Memory

    FlightID++;
    EEPROM.writeInt(45, FlightID ); 
    EEPROM.commit();
    
    flightMode = 1; //flight mode is activated.
    writeMode = 1;  //write to flash memory is enabled.
    storageArrayWrite = 1; //writing the last 50 data to the flash memory before the flight has been activated.
    riseAccelStat=1; //measuring average acceleration from the ground to the apogee was started.    
    flightdata.apogeeTime = millis(); //the time to reach apogee has been started.
  } 
  
}

//flight algorithm
void flightAlgorithm() {
  //If it exceeds 20 meters, the protection of pyro1 is removed.
  if (altitude > 20 && !pyroProt1) {
    pyroProt1 = 1; //first pyro protection removed.   
  } 

  else if (pyroProt1 && !machLock && !pyroProt2) {
    //apogee detected
    if (flightdata.maxAltitude - altitude > 1&&!apogeeCheck) {
      pyroActivationTime=micros(); //the time from the apogee until the first pyro explodes is started.
      apogeeCheck=1; // apogee detect 
      flightdata.apogeeTime = (millis() - flightdata.apogeeTime)/1000; // time to reach apogee has been changed to seconds.
      flightdata.descendTime = millis(); //time of fall was started.
      riseAccelStat=0;   //measuring average acceleration from the ground to the apogee was finished. 
      flightdata.AverageRiseAccel=flightdata.AverageRiseAccel/riseAccelCount; //calculated average acceleration from ground to apogee
      fallAccelStat=1;//measuring average acceleration from the apogee to the ground was started.  
    }
    //pyro1 is activated if it is dropped from the maximum height
    if (flightdata.maxAltitude - altitude > drogueDifference) {
      pyro1Fire(); //the first pyro is activated.
      pyroActivationTime=(micros()-pyroActivationTime)/1000; //the time from the apogee to the first pyro explosion was calculated and converted to milliseconds.
      pyroProt2 = 1;  //second pyro protection removed. 
      apfallSpeedStart = 1; //the average velocity measurement data from the apogee to the main parachute is started.
      flightdata.minAltitude = flightdata.maxAltitude;//the lowest altitude, highest altitude data was thrown so that the algorithm would not be surprised. It will update itself every second as there is a decline. Do not change this part.
    }
  } 
  //pyro 2 is activated if pyro 2 protection is lifted and altitude is suitable for main parachute
  else if (!groundMode && pyroProt2 && (altitude - mainPar) < 5) {
    pyro2Fire();  //the second pyro is activated.
    apfallSpeedStart = 0; //the average velocity measurement data from the apogee to the main parachute is finished.
    mainfallSpeedStart = 1; //the average velocity measurement data from the main to the ground is started.
    groundMode = 1; //ground mode protection removed. 
    flightdata.apogeeFallSpeed = flightdata.apogeeFallSpeed / apfallSpeedCount; // the average velocity from the apogee to the main parachute was calculated.

  } 
  //landing is detected
  else if (groundMode) {
    //If there is a jump in altitude and the gyro sensors are inactive, a descent will occur.
    if ((flightdata.minAltitude - altitude < 0)) { //since the rocket will rise a little as soon as it hits the ground, it is used to switch to ground mode.
      //main fall speed calculation
      mainfallSpeedStart = 0; //the average velocity measurement data from the main to the ground is finished.
      flightdata.mainFallSpeed = flightdata.mainFallSpeed / mainfallSpeedCount; //the average velocity from the main parachute to ground  was calculated.
      flightdata.descendTime = (millis() - flightdata.descendTime)/1000; //the time to fall from the apogee to the ground was calculated and converted to seconds.
      flightdata.flightTime = flightdata.descendTime + flightdata.apogeeTime; //total flight time      
      fallAccelStat=0;//measuring average acceleration from the apogee to the ground was finished. 
      flightdata.AverageFallAccel=flightdata.AverageFallAccel/fallAccelCount; //measuring average acceleration from the apogee to the ground calculated. 
      //pyros are shutting down
      pyroProt1 = 0; //the first pyro protection has been reactivated.
      pyroProt2 = 0; //the second pyro protection has been reactivated.
      pyro1Off();    //the first pyro ignition function has been turned off.
      pyro2Off();    //the second pyro ignition function has been turned off.   
            
      //statistics data are written
      if(measurementSystem) flightdata.maxAltitude= flightdata.maxAltitude*3.2808399; //since the flight algorithm is metric, the maximum altitude is constantly calculated as meric. If imperial is selected, other data are converted in advance, only the max altitude is converted to the imperial system here. 
      
      statData =String(flightdata.maxAltitude,3) + ";" + String(flightdata.maxSpeed,3)+ ";" +String(flightdata.maxAccel,3)+";" + String(flightdata.apogeeTime) 
      + ";" + String(flightdata.apogeeFallSpeed,3) + ";" + String(flightdata.mainFallSpeed,3) + ";" + String(flightdata.descendTime)
      + ";" + String(pyroActivationTime) + ";" + String(flightdata.AverageRiseAccel,3)+ ";" +String(flightdata.AverageFallAccel,3)+ ";" +String(flightdata.flightTime)+ "\n";
      
      statData.replace(".", ",");
      
      //Statistics data is saved in eeprom in order to be able to access it later on the web server.
      EEPROM.writeFloat(4,  flightdata.maxAltitude);
      EEPROM.writeFloat(8,  flightdata.maxSpeed);
      EEPROM.writeFloat(12, flightdata.maxAccel);
      EEPROM.writeFloat(16, flightdata.apogeeTime);
      EEPROM.writeFloat(20, flightdata.apogeeFallSpeed);
      EEPROM.writeFloat(24, flightdata.mainFallSpeed);
      EEPROM.writeFloat(28, flightdata.descendTime);
      EEPROM.writeFloat(32, flightdata.AverageRiseAccel);
      EEPROM.writeFloat(36, flightdata.AverageFallAccel);
      EEPROM.writeFloat(40, pyroActivationTime);
      EEPROM.writeFloat(49, flightdata.flightTime);  
      EEPROM.write(44,   measurementSystem);           
                  
      EEPROM.commit();

      vTaskDelay(100); //waiting while the second core operation is done. do not change.
      writeLastData(); //the last data remaining in the temporary memory is written to the flash.
      writeStatCheck = 1; //removed protection for writing statistics data to flash memory.
      while (writeStatCheck) vTaskDelay(1);
     
/*    String changeFileName; // if you want you can change flight name 
      changeFileName ="/New-Flight.csv";    
      char Buf[60];
      changeFileName.toCharArray(Buf, changeFileName.length()+1);
      renameFile(SPIFFS, "/Flight3.csv",Buf); */
      
      flightMode = 0; //flight mode is deactivated.
      writeMode = 0;  //flash memory write mode is deactivated.
      groundMode=0;   //ground mode is deactivated.
      groundModee =1; //ground mode is deactivated.
      vTaskDelay(100); //don't change for second core
    }
  }
  if(riseAccelStat){ //average velocity function calculated from ground to apogee
    flightdata.AverageRiseAccel+=accelMs5611;
    riseAccelCount++; 
  }
  if(fallAccelStat){ //average velocity function calculated from apogee to ground
    flightdata.AverageFallAccel+=accelMs5611;
    fallAccelCount++;
  }
  //calculating the apogee fall speed
  if (apfallSpeedStart) { //average velocity function calculated from apogee to main parachute
    flightdata.apogeeFallSpeed += flightdata.Ms5611_data[3];
    apfallSpeedCount++;
  }
  //calculating the main fall speed
  if (mainfallSpeedStart) { //average velocity function calculated from main parachute to ground
    flightdata.mainFallSpeed += flightdata.Ms5611_data[3];
    mainfallSpeedCount++;
  }

  float Gama = 1.4; //fixed value
  float R = 287.05; //fixed value
  float T = flightdata.Ms5611_data[0] + 273;  //convert to kelvin
  //mach lock protection
  if (sqrt(Gama * R * T) <= (abs(speed) / 0.93969)) { //machlock calculation function
    machLock = 1;
  } else
    machLock = 0;
}

//pin settings
void setPins() {
  pinMode(pyro1, OUTPUT);
  pinMode(pyro2, OUTPUT);
  pinMode(MS_CS, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  digitalWrite(pyro1, 0);
  digitalWrite(pyro2, 0);
  digitalWrite(MS_CS, 1);
  digitalWrite(buzzer, 0);
  digitalWrite(led1, 0);
  digitalWrite(led2, 0);
  pinMode(pushButton, INPUT_PULLUP);
  
}

//flash settings
void configFlash() {
  if (!SPIFFS.begin(1)) {
    Serial.println("LITTLEFS Mount Failed");
    while (1)
      ;
  }
  flashCreatPart();
  vector.setStorage(storage_array);
  EEPROM.begin(512);
  //If the data is stored in the eeprom, it reads from the eeprom.  
  

  if ( (EEPROM.readFloat(0)>0) ||(EEPROM.read(44)>=0) ) {  // the function where the main parachute and measurement system are updated
     if(EEPROM.readFloat(0)>0) mainPar = EEPROM.readFloat(0);   // ıt was updated in this way so that when the eeprom is reset, it writes the "nan" value and does not get an error.
     if(EEPROM.read(44)<3) measurementSystem = EEPROM.read(44); // ıt was updated in this way so that when the eeprom is reset, it writes the value "255" and does not get an error.
  }

  if(measurementSystem) mainPar=  mainPar/3.2808399; // Parachute opening algorithm was made according to metric order. Do not remove this. If it is removed an error is made when opening the parachute when the measurment system is changed.
  if(mainPar>30000) mainPar=30000; //the main parachute max altitude was limited.
  
  FlightID= EEPROM.readInt(45);
  
  calculateFillRate(); //calculate flash fill rate
    
  Serial.print("Main Parachute Altitude:");
  Serial.println(mainPar);

  Serial.print("Measurment System:");
  Serial.println(measurementSystem);

  Serial.print("Flash Memory Fillrate:");
  Serial.println(fillRate);
  
  Serial.print("Flight Number:");
  Serial.println(FlightID);
  
}
//ms5611 settings
void configMs5611() {
  SPI.begin(18,19,23,5);
  MS5611.initialize();
  MS5611.calibrateAltitude();
}
//Reading ms5611 data
void readMs5611() {

  MS5611.updateData();
  MS5611.updateCalAltitudeKalman();
  MS5611.updateSpeedKalman();
  flightdata.Ms5611_data[0] = MS5611.getTemperature_degC();  //celsius
  flightdata.Ms5611_data[1] = MS5611.getPressure_mbar();     //bar
  flightdata.Ms5611_data[2] = MS5611.getAltitudeKalman();    //meters
  altitude = flightdata.Ms5611_data[2];                      //meters
  flightdata.Ms5611_data[3] = speedKalmanFilter.updateEstimate(MS5611.getVelMs());  //meter/second  
     
  speed = flightdata.Ms5611_data[3];  //meter/second
  accelMs5611TimerNew = millis();
  accelMs5611 = (((speed - speedOld) * 1000) / (accelMs5611TimerNew - accelMs5611TimerOld)) / 9.81;
  accelMs5611TimerOld = millis();
  accelMs5611 = accelKalmanFilter.updateEstimate(accelMs5611);  //g
  speedOld = speed;

  if (measurementSystem == 0) {
    flightdata.Ms5611_data[3] = flightdata.Ms5611_data[3] * 3.6;           //km/hour
  } else {
    flightdata.Ms5611_data[0] = (flightdata.Ms5611_data[0]* 1.8f) + 32;   //fahrenheit  
    flightdata.Ms5611_data[2] = flightdata.Ms5611_data[2]* 3.2808399;     //feet      
    flightdata.Ms5611_data[3] = flightdata.Ms5611_data[3]* 2.24;          //mph
  }
  if (altitude > flightdata.maxAltitude) flightdata.maxAltitude = altitude; //max altitude
  if (altitude < flightdata.minAltitude) flightdata.minAltitude = altitude; //min altitude
  if (abs(flightdata.Ms5611_data[3]) > abs(flightdata.maxSpeed)) flightdata.maxSpeed = flightdata.Ms5611_data[3]; //max speed
  if (abs(accelMs5611) > abs(flightdata.maxAccel)) flightdata.maxAccel = accelMs5611; //max acceleration
  flightdata.unixTime = rtc.getEpoch(); //unixtime
}
//Reading voltage data
void readVoltage() {
  flightdata.battery = readBatt(); // read battery voltage

  pyroStatArr[0] = pyro1Read(); //short circuit check information of first pyro
  pyroStatArr[1] = pyro2Read(); //short circuit check information of second pyro
  pyroStatArr[2] = pyroFire1;   //information that the first pyro is ignition 
  pyroStatArr[3] = pyroFire2;   //information that the first pyro is ignition 
  pyroStatArr[4] = machLock;    //information that the machlock 

  pyroStat = 0;
  for (int i = 0; i < 5; i++) {  //adding pyro and machlock values ​​to string
    pyroStat += pyroStatArr[i] * pow(2, i);
  }
}
//fire pyro1
void pyro1Fire() { //first pyro activation function
  if (pyroProt1) {
    digitalWrite(pyro1, HIGH);
    pyroFire1 = 1;
  }
}
//Off pyro1
void pyro1Off() { //first pyro deactivation function
  digitalWrite(pyro1, LOW);
}
//read pyro1
bool pyro1Read() { //short circuit check information of first pyro
  if ((analogRead(pyro1S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
//fire pyro2
void pyro2Fire() { //second pyro activation function
  if (pyroProt2) {
    digitalWrite(pyro2, HIGH);
    pyroFire2 = 1;
  }
}
//Off pyro2
void pyro2Off() { //second pyro deactivation function
  digitalWrite(pyro2, LOW);
}
//read pyro2
bool pyro2Read() { //short circuit check information of second pyro
  if ((analogRead(pyro2S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
//led1 on
void led1On() { //led 1 activation function
  digitalWrite(led1, HIGH);
}
//led1 off
void led1Off() { //led 1 deactivation function
  digitalWrite(led1, LOW);
}
//led2 on
void led2On() { //led 2 activation function
  digitalWrite(led2, HIGH);
}
//led2 off
void led2Off() { //led 2 deactivation function
  digitalWrite(led2, LOW);
}
//Toggle buzzer
void buzzerToggle(int loop, int delayms) { //buzzer toggle function
  for (int p = 0; p < loop; p++) {
    digitalWrite(buzzer, HIGH);
    delay(delayms);
    digitalWrite(buzzer, LOW);
    delay(delayms);
  }
}
//read battery
double readBatt() { // read battery voltage
  double reading = analogRead(batt);  // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
 
  if (reading < 1 || reading > 4095) return 0;   
    reading = (reading/4095.0)* 6.6*3.3;
  double volt = 471.3768 + (0.9500909 - 471.3768)/(1 + pow((reading/423.8985),1.013072)); // quadratic function for calculate battery voltage
  
  if(volt <= 2.3)return 0;  //added to not show dead voltage of battery.  
  return volt;
}
//creation of buffers for flash
void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    addDataString(All_data1[pre_buffer_memory_counter]);

    if (pre_buffer_memory_counter == tempData-1) {
      pre_buffer_memory_counter = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      pre_buffer_memory_counter++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    addDataString(All_data2[pre_buffer_memory_counter]);
    if (pre_buffer_memory_counter == tempData-1) {
      pre_buffer_memory_counter = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
     pre_buffer_memory_counter++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    addDataString(All_data3[pre_buffer_memory_counter]);
    if (pre_buffer_memory_counter == tempData-1) {
      pre_buffer_memory_counter = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      pre_buffer_memory_counter++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    addDataString(All_data4[pre_buffer_memory_counter]);
    if (pre_buffer_memory_counter == tempData-1) {
      pre_buffer_memory_counter = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      pre_buffer_memory_counter++;
    }
  }
}
//transferring the remaining data to the buffer memory when the flight is completed
void writeLastData() {
  if (dataCheck == 0) {
    flashWriteData(All_data1, pre_buffer_memory_counter);
    file.close();
  } else if (dataCheck == 1) {
    flashWriteData(All_data2, pre_buffer_memory_counter);
    file.close();
  } else if (dataCheck == 2) {
    flashWriteData(All_data3, pre_buffer_memory_counter);
    file.close();
  } else if (dataCheck == 3) {
    flashWriteData(All_data4, pre_buffer_memory_counter);
    file.close();
  }
}
//Adds current data to data string
void addDataString(String &Data) {
  Data = String(flightdata.Ms5611_data[0],3) + ";" + String(flightdata.Ms5611_data[1],3) + ";" + String(flightdata.Ms5611_data[2],3) + ";" + String(flightdata.Ms5611_data[3],3) + ";" + String(accelMs5611,3) + ";";
  Data +=  String(flightdata.battery,3) + ";" + String(pyroStat) + ";" + String(flightdata.unixTime) + "\n";
  Data.replace(".", ",");
  Serial.println(Data);
}
//buffers are sequentially copied to flash and saved
void flashWrite() {

  if (storageArrayWrite) {
    storageArrayWrite = 0;
    flashWriteFirstData(storage_array);
  }
  if (flashCheck1 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1, tempData);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2, tempData);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3, tempData);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4, tempData);
    writeCheck4 = 0;
  }
  if (writeStatCheck) {
    writeStat();
    writeStatCheck = 0;
  }
}
//statistics data is written to flash
void writeStat() {
  appendFile(SPIFFS, Flight_Name.c_str(), headerStat);
  appendFile(SPIFFS, Flight_Name.c_str(), statData);
}
//flight start data is written to flash
void flashWriteFirstData(String a[ELEMENT_COUNT_MAX]) {
  file = SPIFFS.open(Flight_Name.c_str(), FILE_APPEND);
  for (int j = 0; j < ELEMENT_COUNT_MAX; j++) {
    file.print(a[j]);
  }
  file.close();
}
//buffer memory is written to flash
void flashWriteData(String a[tempData], uint8_t wCounter) {
  if (memory == 0) {
    file = SPIFFS.open(Flight_Name.c_str(), FILE_APPEND);
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    memory++;
  }

  else if (memory < tempData-1) {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    memory++;
  }

  else {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    file.close();
    memory = 0;
  }
}
//read flash
void flashRead() {
  readFile(SPIFFS, Flight_Name.c_str());
}
//clean flash
void flashClean() {

  deleteFile(SPIFFS, Flight_Name.c_str());
}
//creat flash part 
void flashCreatPart() {
  appendFile(SPIFFS, Flight_Name.c_str(), All_data_flash1[0]);
}
//list dir
void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}
//--------------------------------------------------------------------------- 
// create dir
void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}
//--------------------------------------------------------------------------- 
//remove dir
void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}
//--------------------------------------------------------------------------- 
// read file
void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}
//--------------------------------------------------------------------------- 
//write file
void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}
//--------------------------------------------------------------------------- 
//append file
void appendFile(fs::FS &fs,const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}
//--------------------------------------------------------------------------- 
// get direction
void getDir(fs::FS &fs, const char * dirname, uint8_t levels) {

  File root = fs.open(dirname);
  File file = root.openNextFile();

  while (file) {
    if (file.isDirectory()) {
      directoryName[countDirecName] = String(file.name());
      countDirecName++;
      if (levels) {
        getDir(fs, file.name(), levels - 1);
      }
    }
    else {
      fileName[countFileName] = String(file.name());
      ++countFileName;
    }
    file = root.openNextFile();
  }
}
//--------------------------------------------------------------------------- 
// delete direction
void deleteDir() {
  for (int j = 0; j < countFileName; j++) {
    int str_len = fileName[j].length() + 1;
    char char_array[str_len];
    fileName[j].toCharArray(char_array, str_len);
    deleteFile(SPIFFS, char_array);
  }

  for (int i = 0; i < countDirecName; i++) {
    int str_len = directoryName[i].length() + 1;
    char char_array[str_len];
    directoryName[i].toCharArray(char_array, str_len);
    removeDir(SPIFFS, char_array);
  }
}
//--------------------------------------------------------------------------- 
void calculateFillRate(){ //calculate flash fill rate
  float totalBytes;
  float usedBytes;
  totalBytes=SPIFFS.totalBytes();
  usedBytes=SPIFFS.usedBytes();
  fillRate=(usedBytes/totalBytes)*100;
}
//--------------------------------------------------------------------------- 
// delete file
void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}
//--------------------------------------------------------------------------- 
//rename file
void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}
//--------------------------------------------------------------------------- 
// WiFi mode
void wifiMode() {
  if (!digitalRead(pushButton)) {
    wifiModeTimer = millis();
    while (!digitalRead(pushButton)) {
      if (millis() - wifiModeTimer > 2000) {        
        //buzzerToggle(2,500);
        led1On();
        writeMode = 0;
        
/*        WiFi.softAP(ssid, password);
        IPAddress IP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(IP);  */
//---------------------------------------------------------------------------        
    WiFi.begin("PLAZACUBES","PlazaCubes@2022");

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
//----------------------------------------------------------------------------
        if (!SPIFFS.begin(true)) {
          Serial.println("SPIFFS initialisation failed...");
          SPIFFS_present = false;
        } else {
          Serial.println(F("SPIFFS initialised... file access enabled..."));
          SPIFFS_present = true;
        }
//----------------------------------------------------------------------------
        // Web Server Commands
        server.on("/", HomePage);
        server.on("/files", FileList);
        server.on("/statistics", Statistics);
        server.on("/pyrotest", PyroTest);
        server.on("/settings", Settings);
        server.begin();
        Serial.println("ROCKFLY-MINI WEB SERVER STARTED");
        uint8_t ledTimer = 0;
        bool ledStat = 0;
        while (1) {
          delay(1);
          if (millis() - start >= 90) {
            start = millis();
            readMs5611();
            readVoltage();
            if (ledTimer > 9 && ledStat) {
              led1On();
              ledStat = 0;
              ledTimer = 0;
            } else if (ledTimer > 9 && !ledStat) {
              led1Off();
              ledStat = 1;
              ledTimer = 0;
            }
            ledTimer++;
          }
          server.handleClient();
        }
      }
    }
  }
} 
void append_page_header() { // html and css code for web server

  webpage = F("<!DOCTYPE html><html lang='en'>");
  webpage += F("<script>function change(value){document.getElementById('option').value=value;}</script>");
  webpage += F("<script>function delAlert(event) {if(confirm('Do you really want to delete the file?')==true){change('delete');document.getElementById('frmfile').submit();}else{event.preventDefault();}}</script>");
  webpage += F("<script>function pyroAlert(event) {if(confirm('Do you really want to fire the pyro?')==true){document.getElementById('frmpyro').submit();}else{event.preventDefault();}}</script>");
  webpage += F("<script>function flashAlert(event) {if(confirm('Do you really want to clear the flash memory?')==true){change('delete');document.getElementById('frmset').submit();}else{event.preventDefault();}}</script>");
  webpage += F("<script>function myFunction1(){\r\n");
  webpage += F("document.getElementById('container').style.display = 'none'\r\n");
  webpage += F("document.getElementById('home').style.background = 'none'\r\n");
  webpage += F("document.getElementById('nav-text').innerHTML = 'RockFLY - Mini Web Server'\r\n");
  webpage += F("document.getElementById('nav-text').style.color = '#FFFFFF'\r\n");
  webpage += F("document.getElementById('navbar').style.background = '#001AFF'\r\n");
  webpage += F("document.getElementById('radio').style.zIndex = '1'}\r\n");
  webpage += F("function myFunction2(){\r\n");
  webpage += F("document.getElementById('container').style.display = 'flex'\r\n");
  webpage += F("document.getElementById('home').style.background = 'rgba(0, 0, 0, 0.8)'\r\n");
  webpage += F("document.getElementById('nav-text').innerHTML = 'RockFLY'\r\n");
  webpage += F("document.getElementById('nav-text').style.color = 'darkblue'\r\n");
  webpage += F("document.getElementById('navbar').style.background = 'rgba(0, 0, 0, 0.8)'\r\n");
  webpage += F("document.getElementById('radio').style.zIndex = '-1'}</script>\r\n");

  webpage += F("<head><title>RockFLY-MiNi Web Server</title>");  // NOTE: 1em = 16px
  webpage += F("<meta name='viewport' content='initial-scale=1.0,width=device-width'>");
  webpage += F("<style>");
  webpage += F(".content,.mini-card{flex-direction:column}*{margin:0}.mini-card{height:100vh;display:flex;");
  webpage += F("justify-content:flex-start;background:0 0}.header{width:100%;height:104px;background:#001aff;display:flex;align-items:center;justify-content:space-between;z-index:2}.header>a{margin-left:32px;");
  webpage += F("width:386px;height:40px;font-family:'SF Pro Display';font-style:normal;font-weight:400;font-size:28px;line-height:125%;color:#fff;text-decoration:none}.content-title,.main_nav a{font-style:normal;");
  webpage += F("line-height:135%;font-family:'SF Pro Display'}.main_nav{display:flex;align-items:center;justify-content:space-between}.main_nav li{list-style-type:none}.main_nav a{margin-right:15px;");
  webpage += F("text-decoration:none;height:19px;font-weight:600;font-size:14px;color:#e2e2e2}.close-button{display:none}.main_nav a.active{padding-bottom:5px;border-bottom:1px solid #f0f0f0}.content{display:flex;");
  webpage += F("justify-content:flex-start;margin-left:32px;margin-top:24px}.content-title{height:32px;font-weight:400;font-size:24px;");
  webpage += F("color:#001aff;margin-bottom:32px;z-index:-1}.content-boxes{display:flex;z-index:-1}.content-boxes>div{display:flex;flex-direction:column;align-items:flex-start;padding:0 24px 12px;");
  webpage += F("gap:12px;max-width:673px;width:100%;height:168px;background:#f0f0f0;margin-right:30px}.content-boxes-titles h3,.content-boxes-titles span,.files-first h4,.files-third button,.files-third h4{height:19px;");
  webpage += F("font-family:'SF Mono';font-weight:600;font-size:14px;font-style:normal;line-height:135%}.content-boxes div h2{height:22px;font-family:'SF Pro Display';font-style:normal;font-weight:700;");
  webpage += F("font-size:16px;line-height:135%;padding:12px 0;color:#001aff}.content-boxes-titles{width:100%;display:flex;justify-content:space-between}.files-first,.files-third{justify-content:space-between;");
  webpage += F("display:flex}.content-boxes-titles span{color:#72767d;text-align:start}.content-boxes-titles h3{color:#1e2c40;padding-top:12px}.files{height:100vh;width:100%}.content-files-box{min-height:201px;");
  webpage += F("max-width:907px;height:100%;width:91%;background:#f0f0f0;border-radius:4px;margin-bottom:32px;gap:12px;padding:24px}.files-first{margin:0 7.5%}.files-first h4{color:#72767d}.content-boxes-line{width:99%;");
  webpage += F("margin:12px 0 24px;border-top:1px solid #000}.files-third{padding-top:12px;margin-bottom:24px}.files-third-web{display:flex;width:50%}.files-third h4{color:#1e2c40;");
  webpage += F("text-align:end}.files-third div{display:flex;justify-content:space-between;margin:0 7.5% 24px}.files-third button{padding:0;border:none;background:0 0;cursor:pointer}.settings,.statistics{display:flex;");
  webpage += F("flex-direction:column;height:100vh}.content-statistics{display:flex;flex-direction:row;flex-wrap:wrap}.content-statistics div{width:20%;margin-bottom:32px;z-index:-1}.content-statistics h3{font-family:'SF Pro Display';");
  webpage += F("font-style:normal;font-weight:700;font-size:16px;color:#001aff;padding-bottom:4px}.content-statistics span{font-family:'SF Mono';font-style:normal;font-weight:600;font-size:14px;color:#1e2c40}.content-settings button span,.content-settings h3{height:22px;");
  webpage += F("font-weight:700;font-size:16px;font-family:'SF Pro Display'}.content-settings{z-index:1}.content-settings h3{font-style:normal;line-height:135%;color:#001aff;margin-bottom:8px}.content-settings-third label,.footer p,.second-input{font-style:normal;");
  webpage += F("font-weight:600;font-size:14px;line-height:135%;color:#1e2c40}.content-settings-fourth,.content-settings-second{display:flex;align-items:center;justify-content:flex-start;text-align:start;margin-right:16px}.second-input{height:51px;");
  webpage += F("width:304px;border-radius:4px;margin-bottom:27px;padding-left:12px;font-family:'SF Mono'}.content-settings-third{display:flex;align-items:center;margin-bottom:34px}.content-settings-third label{height:19px;");
  webpage += F("font-family:'SF Pro Display';margin:0 29px 0 3px}.third-input{background:#1e2c40}.content-settings button{height:51px;max-width:324px;width:100%;background:#001aff;cursor:pointer;border-radius:4px;");
  webpage += F("margin-bottom:32px},cursor: pointer;.content-settings button span{width:298px;font-style:normal;line-height:135%;text-align:center;color:#fff}.footer{display:flex;flex-direction:column;height:83px;");
  webpage += F("margin:auto 32px 0;border-top:1px solid #f0f0f0;justify-content:center;z-index:-1}.mobile-menu{cursor:pointer;display:none}.footer p{height:19px;font-family:'SF Pro Display'}.files-second-item{text-align:center}");
  webpage += F(" @media only screen and (max-width:768px){.header>a{width:180px;height:20px;font-size:15px}.content-statistics div{width:33%}@media only screen and (max-width:576px){.content-boxes,.files-third{flex-direction:column;display:flex}");
  webpage += F(".close-button,.mobile-menu{border:none;cursor:pointer}.mini-card{margin:0}.header{height:69px}.header>a{width:240px;height:20px;font-size:16px;margin-left:16px}.close-button,.mobile-menu span{font-family:'SF Pro Display';");
  webpage += F("font-style:normal;font-weight:600;font-size:14px;line-height:135%}.mobile-menu{display:flex;background-color:Transparent;background-repeat:no-repeat;margin-top:1px}.mobile-menu span{width:36px;height:19px;");
  webpage += F("color:#fff}.content-boxes>div{padding:0 8px 12px;min-width:250px;max-width:575px;width:auto;height:168px;margin:0 0 32px -32px}.content-files-box{min-height:261px;max-width:588px;height:100%;z-index:-1;width:auto;");
  webpage += F("margin-left:-32px}.files-third{margin-bottom:5%}.files-third-web{display:flex;width:auto}.files-third div{margin:0 10% 24px}.content-statistics div{width:40%;margin:0 12px 32px 0}.second-input{max-width:298px;");
  webpage += F(" width:100%}.main_nav{display:none;position:absolute;min-height:97vh;height:auto;top:0;right:0;flex-direction:column;align-items:flex-end;justify-content:flex-start;width:50%;background:#001aff;padding-top:26px}.main_nav li{padding-top:12px}");
  webpage += F(" .close-button{display:block;background-color:Transparent;background-repeat:no-repeat;margin-right:12px;text-decoration:none;height:20px;color:#e2e2e2;padding-bottom:48px}}}");
  webpage += F("</style></head><body>");
  webpage += F(" <div class='mini-card' id='home'><div class='header' id='navbar'>");
  webpage += F("<a id='nav-text' href='/'>RockFLY-Mini Dashboard</a>");
  webpage += F("<ul id='container' class='main_nav'>");
  webpage += F(" <li><button onclick='myFunction1()' class='close-button'>Close</button></li>");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void append_page_footer() {  // Saves repeating many lines of code for HTML page footers        
  webpage += F("<div class='footer'><p>&copy; Appcent Aerospace 2022</p></div></div></body></html>");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// all supporting functions from here...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void HomePage() { // home page html and css code for web server
  SendHTML_Header();
  webpage += F("<meta http-equiv='refresh' content='30'>");
  webpage += F("<li><a href='/' class='active'>System Information</a></li>");
  webpage += F("<li><a href='/files'>Files</a></li>");
  webpage += F("<li><a href='/statistics'>Statistics</a></li>");
  webpage += F("<li><a href='/pyrotest'>Pyro Test</a></li>");
  webpage += F("<li><a href='/settings'>Settings</a></li>");
  webpage += F("</ul><button class='mobile-menu' onclick='myFunction2()'><span>Menu</span></button></div>");
  
  webpage += F("<div class='content'><div class='content-title'>System Information </div>");
  webpage += F("<div><button type=\"button\" onClick=\"window.location.reload();\">Refresh Data</button><br></div><br>");
  webpage += F("<div></div>");
  webpage += F("<div class='content-boxes'><div><div><h2>MS5611</h2></div>");
  webpage += F("<div class='content-boxes-titles'><span>Temp</span><span>Altitude</span><span>Velocity</span></div>");
  webpage += F("<div class='content-boxes-line'></div>");
byte UntSys=EEPROM.read(44);
  webpage += "<div class='content-boxes-titles'><h3>"+String(flightdata.Ms5611_data[0]);
  if (UntSys>0) webpage +=" <sup>o</sup>F"; else webpage +=" <sup>o</sup>C";
  webpage += "</h3><h3>"+String(flightdata.Ms5611_data[2]);
  if (UntSys>0) webpage +=" feet"; else webpage +=" meter";
  webpage +="</h3><h3>"+String(flightdata.Ms5611_data[3]);
  if (UntSys>0) webpage +=" mph"; else webpage +=" km/h";
  webpage += F("</h3></div></div>");
  webpage += F("<div><div><h2>Pyro Information</h2></div>");
  webpage += F("<div class='content-boxes-titles'><span>Batt</span> <span>Pyro 1</span><span>Pyro 2</span></div>");
  webpage += F("<div class='content-boxes-line'></div>");
  webpage += ("<div class='content-boxes-titles'><h3>" + String(flightdata.battery) + " V</h3><h3>" + String(pyroStatArr[1]) + "</h3><h3>" + String(pyroStatArr[0]) + "</h3></div>");
  webpage += F("</div></div><div></div></div>");
  
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();  // stop is needed because no content length was sent
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void FileList() { // file list page html and css code for web server
  if (server.args()>0) {  // Arguments were received
    if (server.arg(0)=="download") {Serial.println("Download"); DownloadFile(server.arg(1));}
    if (server.arg(0)=="delete") {Serial.println("delete"); SPIFFS_file_delete(server.arg(1));}
  } else {
  SendHTML_Header();

  webpage += F("<li><a href='/'>System Information</a></li>");
  webpage += F("<li><a href='/files' class='active'>Files</a></li>");
  webpage += F("<li><a href='/statistics'>Statistics</a></li>");
  webpage += F("<li><a href='/pyrotest'>Pyro Test</a></li>");
  webpage += F("<li><a href='/settings'>Settings</a></li>");
  webpage += F("</ul><button class='mobile-menu' onclick='myFunction2()'><span>Menu</span></button></div>");
  webpage += F("<div class='content'><div class='content-title'>Files</div>");

  webpage += F(" <div class='content-files-box'><div class='files-first'><h4>Name/Type</h4></div> <div class='content-boxes-line'></div>");
  webpage += F("<FORM name='frmfile' id='frmfile' action='/files' method='post'>");  // must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='hidden' name='option' id='option' value=''><br>");
  printDirectoryDownload("/", 0, "Download");              
  webpage += F("</form></div> ");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();  

 }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void DownloadFile(String filename) { //  dowload file page html and css code for web server
  if (SPIFFS_present) {
    Serial.println(filename);
    File download = SPIFFS.open("/" + filename, "r");
    if (download) {
      server.sendHeader("Content-Type", "text/text");
      server.sendHeader("Content-Disposition", "attachment; filename=" + filename);
      server.sendHeader("Connection", "close");
      server.streamFile(download, "application/octet-stream");
      download.close();
    } else
      ReportFileNotPresent("download");
  } else
    ReportSPIFFSNotPresent();
}

//------------------------------------------------------------------------------------------------
void printDirectoryDownload(const char *dirname, uint8_t levels, const char *buttonName) { // download file page html and css code for web server
  File root = SPIFFS.open(dirname);
  if (!root) {
    return;
  }
  if (!root.isDirectory()) {
    return;
  }
  File file = root.openNextFile();
  while (file) {
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if (file.isDirectory() ) {

    } else {
      if(file.size()>0){
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024) fsize = String(bytes) + " B";
      else if (bytes < (1024 * 1024))
        fsize = String(bytes / 1024.0, 3) + " KB";
      else if (bytes < (1024 * 1024 * 1024))
        fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
      else
        fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";  
     String fname=String(file.name());
     fname.replace("/","");
      webpage +="<div class='files-third'><div class='files-third-web'><h4>" + fname + "</h4><h4>" + fsize + "</h4></div><div class='files-third-web'>";      
      webpage +="<button type='submit' name='fname' id='fname' value='" + fname + "' onclick=\"change('download');\" style='color: #001AFF;'>Download</button>";
      webpage +="<button name='dname' id='dname' value='" + fname + "' onclick=\"delAlert(event);\" style='color: #FF0000;'>Delete</button></div></div>";         
      }
    }
    file = root.openNextFile();
  }
  file.close();
}

void Statistics() { // istatistics page html and css code for web server
 SendHTML_Header();

 webpage += F("<li><a href='/'>System Information</a></li>");
 webpage += F("<li><a href='/files'>Files</a></li>");
 webpage += F("<li><a href='/statistics' class='active'>Statistics</a></li>");
 webpage += F("<li><a href='/pyrotest'>Pyro Test</a></li>");
 webpage += F("<li><a href='/settings'>Settings</a></li>");
 webpage += F("</ul><button class='mobile-menu' onclick='myFunction2()'><span>Menu</span></button></div>");
byte UntSys=EEPROM.read(44);

 webpage += F("<div class='content'> <div class='content-title'>Statistics</div><div class='content-statistics'>");
 webpage += "<div><h3>Max Altitude</h3><span>" +  String(EEPROM.readFloat(4)); if (UntSys>0) webpage +=" ft"; else webpage +=" m"; webpage += F("</span></div>");
 webpage += "<div><h3>Max Speed</h3><span>" +  String(EEPROM.readFloat(8)); if (UntSys>0) webpage +=" mph"; else webpage +=" km/h"; webpage += F("</span></div>");
 webpage += "<div><h3>Max Accel</h3><span>" +  String(EEPROM.readFloat(12)) +" g</span></div>";
 webpage += "<div><h3>Apogee Time</h3><span>" +  String(EEPROM.readFloat(16)) +" s</span></div>";
 webpage += "<div><h3>Apogee Fall Speed</h3><span>" +  String(EEPROM.readFloat(20)); if (UntSys>0) webpage +=" mph"; else webpage +=" km/h"; webpage += F("</span></div>");
 webpage += "<div><h3>Main Fall Speed</h3><span>" +  String(EEPROM.readFloat(24)); if (UntSys>0) webpage +=" mph"; else webpage +=" km/h"; webpage += F("</span></div>");
 webpage += "<div><h3>Descend Time</h3><span>" +  String(EEPROM.readFloat(28))+" s</span></div>";
 webpage += "<div><h3>Average Rise Accel</h3><span>" +  String(EEPROM.readFloat(32)) +" g</span></div>";
 webpage += "<div><h3>Average Fall Accel</h3><span>" +  String(EEPROM.readFloat(36)) +" g</span></div>";
 webpage += "<div><h3>Pyro Activation Time</h3><span>" +  String(EEPROM.readFloat(40)) +" ms</span></div>";
 webpage += "<div><h3>Flight Time</h3><span>" +  String(EEPROM.readFloat(49)) +" s</span></div>";
 webpage += F(" </div></div>");
 append_page_footer();
 SendHTML_Content();
 SendHTML_Stop();
}

void Settings() { // settings page html and css code for web server
  if (server.args() > 0) {  // arguments were received
    if (server.hasArg("pnumber")|| server.hasArg("radios") ||server.hasArg("delete") ) {
      SendHTML_Header();
      
      webpage += F("<li><a href='/'>System Information</a></li>");
      webpage += F("<li><a href='/files'>Files</a></li>");
      webpage += F("<li><a href='/statistics'>Statistics</a></li>");
      webpage += F("<li><a href='/pyrotest'>Pyro Test</a></li>");
      webpage += F("<li><a href='/settings' class='active'>Settings</a></li>");
      webpage += F("</ul><button class='mobile-menu' onclick='myFunction2()'><span>Menu</span></button></div>");
      webpage += "<form action='/settings' method='post'>";
      webpage += F("<div class='content'> <div class='content-title'>Settings</div><div class='content-settings'>");

      if (server.arg(2)=="delete") {
      webpage += "<h3>Flash Memory Was Cleaned</h3>";
      webpage += F("<a href='/settings'>[Back]</a><br><br>");               
          getDir(SPIFFS, "/", 1);
          deleteDir();
          calculateFillRate();
      }else{

      webpage += "<h3>Configuration has been saved</h3>";
      webpage += F("<a href='/settings'>[Back]</a><br><br>");
      
      mainPar = server.arg(0).toFloat();
      if(mainPar>0){ 

      EEPROM.writeFloat(0, mainPar);      
      EEPROM.commit();}
                 
      int choosen = server.arg(1).toInt();
      if(choosen>0){
      if(choosen==2) measurementSystem=0;
      else if(choosen==1) measurementSystem=1; 
      
      EEPROM.write(44, measurementSystem);      
      EEPROM.commit();}
    }
        
      append_page_footer();
      SendHTML_Content();
      SendHTML_Stop();
    }

  } else {
    SendHTML_Header();

    webpage += F("<li><a href='/'>System Information</a></li>");
    webpage += F("<li><a href='/files'>Files</a></li>");
    webpage += F("<li><a href='/statistics'>Statistics</a></li>");
    webpage += F("<li><a href='/pyrotest'>Pyro Test</a></li>");
    webpage += F("<li><a href='/settings' class='active'>Settings</a></li>");
    webpage += F("</ul><button class='mobile-menu' onclick='myFunction2()'><span>Menu</span></button></div>");
    webpage += "<form name='frmset' id='frmset'  action='/settings' method='post'>";
    webpage += F("<div class='content'> <div class='content-title'>Settings</div><div class='content-settings'>");
byte UntSys=EEPROM.read(44);
    webpage += F("<div class='content-settings'><div><h3>Main Parachute:</h3></div>");

    webpage += "<div class='content-settings-second'><input class='second-input' type='number' name='pnumber' id='pnumber' value='"+String(int(mainPar))+"' min='0'  />";
    webpage += "<span id='unittype' style='margin-top:-25px;margin-left:-80px;'>";
    if (UntSys==0){webpage +="meter";}else{webpage +="feet";}
    webpage += "</span></div>";
              
    webpage += "<div class='content-settings-third'><input class='third-input' type='radio' id='imperial' name='radios' value='1' onclick=\"document.getElementById('unittype').innerText ='feet';\" ";   
    if (UntSys>0)webpage +="checked"; 
    webpage += "/><label for='imperial'>Imperial</label><input class='third-input' type='radio' id='metric' name='radios' value='2' onclick=\"document.getElementById('unittype').innerText ='meter';\" ";
    if (UntSys==0)webpage +="checked";
    webpage += "/><label for='metric'>Metric</label></div>";   
    
    webpage += "<div class='content-settings-fiveth'><button type='submit' class='fiveth-input'>";  //test kodu için yapıldı normal yapısı daha farklı ve patlatmadan önce soru sorulacak.  
    webpage += "<span>Save</span></button></div></div>";
    webpage += F("<input type='hidden' name='option' id='option' value=''>");
    webpage += F("<div class='content-settings'><div><h3>Flash Memory:</h3></div>");
    webpage += "<div class='content-settings-third'><label>Flash Memory Usage : % "+String(fillRate)+"</label></div>";
    webpage += "<div class='content-settings-fiveth'><button type='button' class='fiveth-input' onclick=\"flashAlert('delete');\">";  //test kodu için yapıldı normal yapısı daha farklı ve patlatmadan önce soru sorulacak.  
    webpage += "<span>Flash Memory Clear</span></button></div></div>"; 
    webpage += "</form>";

    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void PyroTest() { //  pyro test page html and css code for web server
  if (server.args() > 0) {  // arguments were received
    if (server.hasArg("pyro")) {
      SendHTML_Header();
      webpage += F("<li><a href='/'>System Information</a></li>");
      webpage += F("<li><a href='/files'>Files</a></li>");
      webpage += F("<li><a href='/statistics'>Statistics</a></li>");
      webpage += F("<li><a href='/pyrotest' class='active'>Pyro Test</a></li>");
      webpage += F("<li><a href='/settings'>Settings</a></li>");
      webpage += F("</ul><button class='mobile-menu' onclick='myFunction2()'><span>Menu</span></button></div>");
      webpage += F("<form name='frmpyro' id='frmpyro' action='/pyrotest' method='post'>");
      webpage += F("<div class='content'> <div class='content-title'>Pyro Test</div><div class='content-settings'>");

      webpage += F("<h3>Pyro has been fired</h3>");
      webpage += F("<a href='/pyrotest'>[Back]</a><br><br>"); 
    
      append_page_footer();
      SendHTML_Content();
      SendHTML_Stop();
      
      if (server.arg(0)=="1") { 
          pyroProt1=1;
          pyro1Fire();
          pyroProt1=0;
                
          Serial.println("drogueTest");
          delay(200);
          pyro1Off();
        }
     if (server.arg(0)=="2") {
          pyroProt2=1;
          pyro2Fire();
          pyroProt2=0;
          
          Serial.println("mainTest");
          delay(200);
          pyro2Off();  
        }
    }

  } else {
    SendHTML_Header();
    webpage += F("<li><a href='/'>System Information</a></li>");
    webpage += F("<li><a href='/files'>Files</a></li>");
    webpage += F("<li><a href='/statistics'>Statistics</a></li>");
    webpage += F("<li><a href='/pyrotest' class='active'>Pyro Test</a></li>");
    webpage += F("<li><a href='/settings'>Settings</a></li>");
    webpage += F("</ul><button class='mobile-menu' onclick='myFunction2()'><span>Menu</span></button></div>");
    webpage += "<form action='/pyrotest' method='post'>";
    webpage += F("<div class='content'> <div class='content-title'>Pyro Test</div><div class='content-settings'>");

    webpage += F("<div class='content-settings-third'>");
    webpage += "<input class='third-input' type='radio' id='drogue' name='pyro' value='1' checked/>";   //test kodu için yapıldı normal yapısı daha farklı ve patlatmadan önce soru sorulacak.
    webpage += "<label for='drogue'>Drogue Pyro</label>";    
    webpage += "<input class='third-input' type='radio' id='main' name='pyro' value='2'/>"; //test kodu için yapıldı normal yapısı daha farklı ve patlatmadan önce soru sorulacak.
    webpage += "<label for='main'>Main Pyro</label></div>";
    webpage += "<div class='content-settings-fiveth'>";    
    webpage += "<button onclick=\"pyroAlert(event);\" class='fourth-input'>";  //test kodu için yapıldı normal yapısı daha farklı ve patlatmadan önce soru sorulacak.  
    webpage += "<span>Fire</span></button></div>";
    webpage += "</form>";

    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SPIFFS_file_delete(String filename) {  //  delete file page html and css code for web server
  if (SPIFFS_present) {
    if (SPIFFS.remove("/" + filename)) {
      Serial.println(F("File deleted successfully"));
      webpage += "<!DOCTYPE html><html lang='en'><head><meta http-equiv=\"refresh\" content=\"0; url=/files\" /></head></html>";
     
    } else {
      webpage += "<!DOCTYPE html><html lang='en'><head><meta http-equiv=\"refresh\" content=\"0; url=/files\" /></head></html>";
      Serial.println(F("File was not deleted - error"));

    }
    SendHTML_Content();
    SendHTML_Stop();
  } else
    ReportSPIFFSNotPresent();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Header() { // header  html and css code for web server
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", "");  // Empty content inhibits Content-length header so we have to close the socket ourselves.
  append_page_header();
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Content() { // html content
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Stop() { // stop html
  server.sendContent("");
  server.client().stop();  // Stop is needed because no content length was sent
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportSPIFFSNotPresent() { // spiffs card present
  SendHTML_Header();
  webpage += F("<h3>No SPIFFS Card present</h3>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportFileNotPresent(String target) { // File does not exist
  SendHTML_Header();
  webpage += F("<h3>File does not exist</h3>");
  webpage += F("<a href='/");
  webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
