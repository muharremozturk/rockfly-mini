#include "FS.h"
#include <SPI.h>
#include <Wire.h>
#include "LITTLEFS.h"
#include "ms5611_spi.h"
#include <Vector.h>
#include <WiFi.h>  // Built-in
#include <WebServer.h>
#include <ESP32Time.h>
#include "EEPROM.h"
#include <SimpleKalmanFilter.h>

uint8_t timeZone = 3; //Changing the time zone
float mainPar = 20;  //main parachute replacement 
bool measurementSystem = 0; //change the measurement system , chose only 0 or 1 -> "0" is metric system , "1" is imperial system 
uint8_t drogueDifference =2; //how many meters from the drogue will the parachute open

//flight and write modes must be turned on together
bool flightMode = 0; //turn on flight mode
bool writeMode = 0; //turn on write mode

const uint8_t tempData =20; //buffer size

//SimpleKalmanFilter pressureKalmanFilter2(1, 1, 0.01);
SimpleKalmanFilter speedKalmanFilter(1, 1, 0.1); // speed filter
SimpleKalmanFilter accelKalmanFilter(1, 1, 0.1); // acceleration filter

ESP32Time rtc; //real time clock

//wifi settings
#define ServerVersion "1.0"
String webpage = "";
#define SPIFFS LITTLEFS
bool SPIFFS_present = false;
const char *ssid = "RockFLY2022"; //wifi id
const char *password = "rockfly2022"; //wifi password
WebServer server(80);

//for pre-flight data
const int ELEMENT_COUNT_MAX = 50;
typedef Vector<String> Elements;
String storage_array[ELEMENT_COUNT_MAX];
Elements vector;


#define buzzer 21 //buzzer io pin  
#define pyro1 16 //pyro1 io pin
#define pyro2 32 //pyro2 io pin
#define pyro1S 39 //pyro1 measurment pin
#define pyro2S 36 //pyro2 measurement pin
#define led1 17 //led1 io pin
#define led2 25 //led2 io expender pin
#define batt 35 //battery measurement pin

uint8_t pyroStat; 
bool pyroStatArr[8]; //pyro state array

float accelMs5611; //ms5611 accel data
long accelMs5611TimerNew, accelMs5611TimerOld; //ms5611 accel timer
float speed; //ms5611 speed data
float speedOld;  
float altitude; //ms5611 altitude data
float mainParChange = 0.00; // Show change main parachute altitude on WiFi-Settings
int FlightID =0; // Number of Flight

//pyro variables
bool pyroFire1 = 0;
bool pyroFire2 = 0;
bool pyroProt1 = 0;
bool pyroProt2 = 0;

bool riseAccelStat=0;
bool fallAccelStat=0;
float riseAccelCount = 0;
float fallAccelCount = 0;
bool apogeeCheck=0;
bool machLock = 0;
bool groundMode = 0;
bool groundModee = 0;
bool apfallSpeedStart = 0;
bool apfallSpeedStop = 0;
unsigned int apfallSpeedCount = 0;
bool mainfallSpeedStart = 0;
bool mainfallSpeedStop = 0;
unsigned int mainfallSpeedCount = 0;
long pyroActivationTime=0;

struct flightData {
  float Ms5611_data[4];        //temp, pressure, speed, accel
  int GPS_time[6];             //time
  float battery;               //battery voltage 
  float maxAltitude;           //max altitude
  float minAltitude;           //min altitude
  float maxSpeed;              //max speed
  float maxAccel;              //max acceleration 
  unsigned long apogeeTime;    //apogee time
  float apogeeFallSpeed;       //apogee fall speed
  float mainFallSpeed;         //main fall speed
  unsigned long descendTime;   //descend time
  unsigned long flightTime;    //flight time
  unsigned long unixTime;      //unix time 
  float AverageRiseAccel;      //average rise acceleration  
  float AverageFallAccel;      //average fall acceleration
  
};
flightData flightdata;

//ms5611 definitions
uint8_t MS_CS = 5;
baro_ms5611 MS5611(MS_CS);

File file;

//temporary buffer data array
String All_data1[tempData];
String All_data2[tempData];
String All_data3[tempData];
String All_data4[tempData];

//temporary buffer data array for flash
String All_data_flash1[tempData];
String All_data_flash2[tempData];
String All_data_flash3[tempData];
String All_data_flash4[tempData];

//flash header 
String header = "Temp;Press;Alt;Speed;Accel;Batt;PyroStat;Time\n";
String headerStat = "MaxAltitude;MaxSpeed;MaxAccel;ApogeeTime;ApogeeFallSpeed;MainFallSpeed;DescendTime;PyroActivationTime(ms);AverageRiseAccel;AverageFallAccel;FlightTime\n";
String statData;

int memory = 0; //buffer memory counter
int i = 0; //pre buffer memory counter

//flash check bits
bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool flashCheck3 = 0;
bool flashCheck4 = 0;

//write check bits
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool writeCheck3 = 0;
bool writeCheck4 = 0;

uint8_t dataCheck = 0;
bool writeStart = 0;
bool error = 0;
bool sendSensorData = 0;
bool sendFrqConfigData = 0;
bool setConOk = 0;
bool sendPrchtConData = 0;

bool storageArrayWrite = 0;
bool setGpsTime = 0;
bool sendgroundData = 0;
bool sendgroundDataStat = 0;
bool sendStat = 0;
bool configFlashCheck = 0;
bool writeStatCheck = 0;

int pushButton = 0; //wifi mode button
unsigned long wifiModeTimer = 0; //

TaskHandle_t Task1; // second core variable

unsigned long start = 0; //for read data timer


void setup() {
  Serial.begin(115200);
  setPins();          //pin settings
  configFlash();      //flash settings
  configMs5611();     //ms5611 settings
  buzzerToggle(5, 500);//star buzzer
  wifiMode();         //wifi mode settings

  //start core 2
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */

}

// Flash recording in core 2
void Task1code(void *pvParameters) {

  for (;;) {
    vTaskDelay(1);
    if (writeMode) {
      if (!configFlashCheck) {
        appendFile(SPIFFS, "/Data.csv", header); //write header one times
        configFlashCheck = 1;
      }
      flashWrite(); //write data
    }
  }
}



void loop() {
  vTaskDelay(1);

  if (!flightMode == 1) {
    preFlightAlgorithm(); //Runs the pre-flight algorithm
  } else {

    if (millis() - start >= 30) {
      start = millis();
      readMs5611();       //read ms5611 
      readVoltage();      //read voltage
      flightAlgorithm();  //Runs the flight algorithm
      writeData();        //collects the data in an array and sends it to the 2nd core
    }
  }
}
void preFlightAlgorithm() {
  if (millis() - start >= 90) {
    start = millis();
    if (!groundModee) {  //read data out of ground mode
      readMs5611();     //read ms5611
      readVoltage();    //read voltage
      String data;
      addDataString(data);//add data to string

      //collect 50 data in vector
      if (vector.size() == vector.max_size()) {
        vector.remove(0);
      }
      vector.push_back(data);
    }
  }

  //flight detection
  if ((altitude > 5)&&(!groundModee)) {
    flightMode = 1;
    writeMode = 1;
    storageArrayWrite = 1;
    riseAccelStat=1;    
    flightdata.apogeeTime = millis();
  } 
  
}
//flight algorithm
void flightAlgorithm() {
  //If it exceeds 20 meters, the protection of pyro1 is removed.
  if (altitude > 20 && !pyroProt1) {
    pyroProt1 = 1;
    
  } 

  else if (pyroProt1 && !machLock && !pyroProt2) {
    //apogee detected
    if (flightdata.maxAltitude - altitude > 1&&!apogeeCheck) {
//       Serial.println("APOOGEEEEEE!!!!!!");
      pyroActivationTime=micros();
      apogeeCheck=1;
      flightdata.apogeeTime = (millis() - flightdata.apogeeTime)/1000;
      flightdata.descendTime = millis();
      riseAccelStat=0;
      flightdata.AverageRiseAccel=flightdata.AverageRiseAccel/riseAccelCount;
      fallAccelStat=1;
    }
    //pyro1 is activated if it is dropped from the maximum height
    if (flightdata.maxAltitude - altitude > drogueDifference) {
//      Serial.println("!!!!!!!PYRO-1 AKTİFFFF!!!!!!");
      pyro1Fire();
      pyroActivationTime=(micros()-pyroActivationTime)/1000;
      pyroProt2 = 1;
      apfallSpeedStart = 1;
      sendStat = 1;
      flightdata.minAltitude = flightdata.maxAltitude;
    }
  } 
  //pyro 2 is activated if pyro 2 protection is lifted and altitude is suitable for main parachute
  else if (!groundMode && pyroProt2 && (altitude - mainPar) < 5) {
//    Serial.println("!!!!!!!MAIN PARAŞÜT PYRO-2 AKTİFFFF!!!!!!");
    pyro2Fire();
    apfallSpeedStop = 1;
    mainfallSpeedStart = 1;
    groundMode = 1;
    flightdata.apogeeFallSpeed = flightdata.apogeeFallSpeed / apfallSpeedCount;
    apfallSpeedStart = 0;

  } 
  //landing is detected
  else if (groundMode) {
    //If there is a jump in altitude and the gyro sensors are inactive, a descent will occur.
    if ((flightdata.minAltitude - altitude < 0)) {
      //main fall speed calculation
      
//      Serial.println("!!!!!!!GROUND MOD AKTİF!!!!!");
      mainfallSpeedStop = 1;
      flightdata.mainFallSpeed = flightdata.mainFallSpeed / mainfallSpeedCount;
      mainfallSpeedStart = 0;
      flightdata.descendTime = (millis() - flightdata.descendTime)/1000;
      flightdata.flightTime = flightdata.descendTime + flightdata.apogeeTime;
      
      fallAccelStat=0;
      flightdata.AverageFallAccel=flightdata.AverageFallAccel/fallAccelCount;
      //pyros are shutting down
      pyroProt1 = 0;
      pyroProt2 = 0;
      pyro1Off();
      pyro2Off();      
      FlightID++;
      
      //stats are written
      if(measurementSystem) flightdata.maxAltitude= flightdata.maxAltitude*3.2808399; //maximum irtifa imperial olarak düzenlendi. uçuş algoritması metric olduğu için böyle yaptık.
      
      statData =String(flightdata.maxAltitude,3) + ";" + String(flightdata.maxSpeed,3)+ ";" +String(flightdata.maxAccel,3)+";" + String(flightdata.apogeeTime) 
      + ";" + String(flightdata.apogeeFallSpeed,3) + ";" + String(flightdata.mainFallSpeed,3) + ";" + String(flightdata.descendTime)
      + ";" + String(pyroActivationTime) + ";" + String(flightdata.AverageRiseAccel,3)+ ";" +String(flightdata.AverageFallAccel,3)+ ";" +String(flightdata.flightTime)+ "\n";
      
      statData.replace(".", ",");
      
      EEPROM.writeFloat(4,  flightdata.maxAltitude);
      EEPROM.writeFloat(8,  flightdata.maxSpeed);
      EEPROM.writeFloat(12, flightdata.maxAccel);
      EEPROM.writeFloat(16, flightdata.apogeeTime);
      EEPROM.writeFloat(20, flightdata.apogeeFallSpeed);
      EEPROM.writeFloat(24, flightdata.mainFallSpeed);
      EEPROM.writeFloat(28, flightdata.descendTime);
      EEPROM.writeFloat(32, flightdata.AverageRiseAccel);
      EEPROM.writeFloat(36, flightdata.AverageFallAccel);
      EEPROM.writeFloat(40, pyroActivationTime);
      EEPROM.writeFloat(51, flightdata.flightTime);  
      EEPROM.write(44,   measurementSystem);     
      EEPROM.writeInt(45, FlightID ); 
                  
      EEPROM.commit();

      vTaskDelay(100);
      writeLastData();
      writeStatCheck = 1;
      while (writeStatCheck) vTaskDelay(1);
      
      //filename changing with date
      String recordTime;
      recordTime = "/Flight" + String(FlightID)+ "_";
      recordTime +=/*String(rtc.getHour()) + "_" + String(rtc.getMinute()) + "_" +*/ String(rtc.getDay()) + "_" + String(rtc.getMonth() + 1) + "_" + String(rtc.getYear())+ ".csv";
      char Buf[60];
      recordTime.toCharArray(Buf, recordTime.length()+1);
      renameFile(SPIFFS, "/Data.csv",Buf);      
      
      flightMode = 0;
      writeMode = 0;
      groundMode=0;
      groundModee =1;
      vTaskDelay(100);
    }
  }
  if(riseAccelStat){
    flightdata.AverageRiseAccel+=accelMs5611;
    riseAccelCount++;
  }
  if(fallAccelStat){
    flightdata.AverageFallAccel+=accelMs5611;
    fallAccelCount++;
  }
  //calculating the apogee fall speed
  if (apfallSpeedStart) {
    flightdata.apogeeFallSpeed += flightdata.Ms5611_data[3];
    apfallSpeedCount++;
  }
  //calculating the main fall speed
  if (mainfallSpeedStart) {
    flightdata.mainFallSpeed += flightdata.Ms5611_data[3];
    mainfallSpeedCount++;
  }

  float Gama = 1.4;
  float R = 287.05;
  float T = flightdata.Ms5611_data[0] + 273;  //convert to kelvin
  //mach lock protection
  if (sqrt(Gama * R * T) <= (abs(speed) / 0.93969)) {
    machLock = 1;
  } else
    machLock = 0;
}

//pin settings
void setPins() {
  pinMode(pyro1, OUTPUT);
  pinMode(pyro2, OUTPUT);
  pinMode(MS_CS, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  digitalWrite(pyro1, 0);
  digitalWrite(pyro2, 0);
  digitalWrite(MS_CS, 1);
  digitalWrite(buzzer, 0);
  digitalWrite(led1, 0);
  digitalWrite(led2, 0);
  pinMode(pushButton, INPUT_PULLUP);
  rtc.setTime(0, 0, 0, 9, 6, 2022); //rtc set
}

//flash settings
void configFlash() {
  if (!SPIFFS.begin(1)) {
    Serial.println("LITTLEFS Mount Failed");
    while (1)
      ;
  }
  //flashRead();
  //flashClean();
  flashCreatPart();
  vector.setStorage(storage_array);
  EEPROM.begin(512);
  //If the data is stored in the eeprom, it reads from the eeprom.
  
  if ((EEPROM.readFloat(0)>0) ||(EEPROM.read(44)>=0) ) { //imperial-metric seçim butonu güncel seçilende kalmalı!! default birisinde kalırsa her güncellemede değişir ,adam imperial seçti ama chechbox metric kaldı sonra mainpar güncelledi buton ikisini birden gönderince sıkıntı olmasın bu kısımda
     mainPar = EEPROM.readFloat(0);   
     measurementSystem = EEPROM.read(44);
  }

  if(measurementSystem) mainPar=  mainPar/3.2808399; // Paraşüt açma algoritması metric düzene göre yapıldı. Burayı kaldırmayın. Eğer kaldırılırsa measurment sistem değiştiğinde paraşüt açarken fail edilir.

  FlightID= EEPROM.readInt(45);

  Serial.print("mainpar:");
  Serial.println(mainPar);

  Serial.print("Measurment System:");
  Serial.println(measurementSystem);
  
  Serial.print("Uçuş Sayısı:");
  Serial.println(FlightID);
  
}
//ms5611 settings
void configMs5611() {
  SPI.begin(18,19,23,5);
  MS5611.initialize();
  MS5611.calibrateAltitude();
}
//Reading ms5611 data
void readMs5611() {

  MS5611.updateData();
  MS5611.updateCalAltitudeKalman();
  MS5611.updateSpeedKalman();
  flightdata.Ms5611_data[0] = MS5611.getTemperature_degC();  //Celsius
  flightdata.Ms5611_data[1] = MS5611.getPressure_mbar();     //bar
  flightdata.Ms5611_data[2] = MS5611.getAltitudeKalman();    //meters
  altitude = flightdata.Ms5611_data[2];                      //meters
  flightdata.Ms5611_data[3] = speedKalmanFilter.updateEstimate(MS5611.getVelMs());  //meter/second  
     
//  flightdata.Ms5611_data[2] =pressureKalmanFilter2.updateEstimate(altitudeData[counteraltitude]); ;
//  altitude = flightdata.Ms5611_data[2]; 
  speed = flightdata.Ms5611_data[3];  //meter/second
  accelMs5611TimerNew = millis();
  accelMs5611 = (((speed - speedOld) * 1000) / (accelMs5611TimerNew - accelMs5611TimerOld)) / 9.81;
  accelMs5611TimerOld = millis();
  accelMs5611 = accelKalmanFilter.updateEstimate(accelMs5611);  //g
  speedOld = speed;
//  counteraltitude++;

  if (measurementSystem == 0) {
    flightdata.Ms5611_data[3] = flightdata.Ms5611_data[3] * 3.6;           //km/hour
  } else {
    flightdata.Ms5611_data[0] = (flightdata.Ms5611_data[0]* 1.8f) + 32;   //fahrenheit  
    flightdata.Ms5611_data[2] = flightdata.Ms5611_data[2]* 3.2808399;     //feet      
    flightdata.Ms5611_data[3] = flightdata.Ms5611_data[3]* 2.24;          //mph
  }
  if (altitude > flightdata.maxAltitude) flightdata.maxAltitude = altitude;
  if (altitude < flightdata.minAltitude) flightdata.minAltitude = altitude;
  if (abs(flightdata.Ms5611_data[3]) > abs(flightdata.maxSpeed)) flightdata.maxSpeed = flightdata.Ms5611_data[3];
  if (abs(accelMs5611) > abs(flightdata.maxAccel)) flightdata.maxAccel = accelMs5611;
  flightdata.unixTime = rtc.getEpoch();
}
//Reading voltage data
void readVoltage() {
  flightdata.battery = readBatt();

  pyroStatArr[0] = pyro1Read();
  pyroStatArr[1] = pyro2Read();
  pyroStatArr[2] = pyroFire1;
  pyroStatArr[3] = pyroFire2;
  pyroStatArr[4] = machLock;

  pyroStat = 0;
  for (int i = 0; i < 5; i++) {
    pyroStat += pyroStatArr[i] * pow(2, i);
  }
  //Serial.println(pyroStat);
}
//fire pyro1
void pyro1Fire() {
  if (pyroProt1) {
    digitalWrite(pyro1, HIGH);
    pyroFire1 = 1;
  }
}
//Off pyro1
void pyro1Off() {
  digitalWrite(pyro1, LOW);
}
//read pyro1
bool pyro1Read() {
  if ((analogRead(pyro1S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
//fire pyro2
void pyro2Fire() {
  if (pyroProt2) {
    digitalWrite(pyro2, HIGH);
    pyroFire2 = 1;
  }
}
//Off pyro2
void pyro2Off() {
  digitalWrite(pyro2, LOW);
}
//read pyro2
bool pyro2Read() {
  if ((analogRead(pyro2S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
//led1 on
void led1On() {
  digitalWrite(led1, HIGH);
}
//led1 off
void led1Off() {
  digitalWrite(led1, LOW);
}
//led2 on
void led2On() {
  digitalWrite(led2, HIGH);
}
//led2 off
void led2Off() {
  digitalWrite(led2, LOW);
}
//Toggle buzzer
void buzzerToggle(int loop, int delayms) {
  for (int p = 0; p < loop; p++) {
    digitalWrite(buzzer, HIGH);
    delay(delayms);
    digitalWrite(buzzer, LOW);
    delay(delayms);
  }
}
//read battery
double readBatt() {
  double reading = analogRead(batt);  // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
 
  if (reading < 1 || reading > 4095) return 0;   
    reading = (reading/4095.0)* 6.6*3.3;
  return 471.3768 + (0.9500909 - 471.3768)/(1 + pow((reading/423.8985),1.013072));
}
//creation of buffers for flash
void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    addDataString(All_data1[i]);
    //Serial.println(All_data1[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    addDataString(All_data2[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
      i++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    addDataString(All_data3[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      i++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    addDataString(All_data4[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}
//transferring the remaining data to the buffer memory when the flight is completed
void writeLastData() {
  if (dataCheck == 0) {
    flashWriteData(All_data1, i);
    file.close();
  } else if (dataCheck == 1) {
    flashWriteData(All_data2, i);
    file.close();
  } else if (dataCheck == 2) {
    flashWriteData(All_data3, i);
    file.close();
  } else if (dataCheck == 3) {
    flashWriteData(All_data4, i);
    file.close();
  }
}
//Adds current data to data string
void addDataString(String &Data) {
  Data = String(flightdata.Ms5611_data[0],3) + ";" + String(flightdata.Ms5611_data[1],3) + ";" + String(flightdata.Ms5611_data[2],3) + ";" + String(flightdata.Ms5611_data[3],3) + ";" + String(accelMs5611,3) + ";";
  Data +=  String(flightdata.battery,3) + ";" + String(pyroStat) + ";" + String(flightdata.unixTime) + "\n";
  Data.replace(".", ",");
  Serial.println(Data);
}
//buffers are sequentially copied to flash and saved
void flashWrite() {

  if (storageArrayWrite) {
    storageArrayWrite = 0;
    flashWriteFirstData(storage_array);
  }
  if (flashCheck1 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1, tempData);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2, tempData);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3, tempData);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4, tempData);
    writeCheck4 = 0;
  }
  if (writeStatCheck) {
    writeStat();
    writeStatCheck = 0;
  }
}
//statistics data is written to flash
void writeStat() {
  appendFile(SPIFFS, "/Data.csv", headerStat);
  appendFile(SPIFFS, "/Data.csv", statData);
}
//flight start data is written to flash
void flashWriteFirstData(String a[ELEMENT_COUNT_MAX]) {
  file = SPIFFS.open("/Data.csv", FILE_APPEND);
  for (int j = 0; j < ELEMENT_COUNT_MAX; j++) {
    file.print(a[j]);
  }
  file.close();
}
//buffer memory is written to flash
void flashWriteData(String a[tempData], uint8_t wCounter) {
  if (memory == 0) {
    file = SPIFFS.open("/Data.csv", FILE_APPEND);
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    memory++;
  }

  else if (memory < tempData-1) {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    memory++;
  }

  else {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    file.close();
    memory = 0;
  }
}

void flashRead() {
  readFile(SPIFFS, "/Data.csv");
}
//
void flashClean() {

  deleteFile(SPIFFS, "/Data.csv");
}
void flashCreatPart() {
  appendFile(SPIFFS, "/Data.csv", All_data_flash1[0]);
}
//list dir
void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}
// create dir
void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}
//remove dir
void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}
// read file
void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}
//write file
void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}
//append file
void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}
// delete file
void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}
//rename file
void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}

void wifiMode() {
  if (!digitalRead(pushButton)) {
    wifiModeTimer = millis();
    while (!digitalRead(pushButton)) {
      if (millis() - wifiModeTimer > 2000) {
        //buzzerToggle(2,500);
        led1On();
        writeMode = 0;
        WiFi.softAP(ssid, password);
        IPAddress IP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(IP);  

        if (!SPIFFS.begin(true)) {
          Serial.println("SPIFFS initialisation failed...");
          SPIFFS_present = false;
        } else {
          Serial.println(F("SPIFFS initialised... file access enabled..."));
          SPIFFS_present = true;
        }
        //----------------------------------------------------------------------
        ///////////////////////////// Server Commands
        server.on("/", HomePage);
        server.on("/download", File_Download);
        server.on("/delete", File_Delete);
        server.on("/data", Data);  
        server.on("/statistics", Statistics);
        server.on("/settings", Settings);
        server.begin();
        Serial.println("ROCKFLY-MINI HTTP SERVER STARTED");
        uint8_t ledTimer = 0;
        bool ledStat = 0;
        while (1) {
          delay(1);
          if (millis() - start >= 90) {
            start = millis();
            readMs5611();
            readVoltage();
            if (ledTimer > 9 && ledStat) {
              led1On();
              ledStat = 0;
              ledTimer = 0;
            } else if (ledTimer > 9 && !ledStat) {
              led1Off();
              ledStat = 1;
              ledTimer = 0;
            }
            ledTimer++;
          }
          server.handleClient();
        }
      }
    }
  }
}
void append_page_header() {
  webpage = F("<!DOCTYPE html><html>");
  webpage += F("<script>function change(value){document.getElementById('download').value=value;}</script>");
  webpage += F("<head>");
  webpage += F("<title>File Server</title>");  // NOTE: 1em = 16px
  webpage += F("<meta name='viewport' content='user-scalable=yes,initial-scale=1.0,width=device-width'>");
  webpage += F("<style>");
  webpage += F("body{max-width:65%;margin:0 auto;font-family:arial;font-size:105%;text-align:center;color:blue;background-color:#F7F2Fd;}");
  webpage += F("ul{list-style-type:none;margin:0.1em;padding:0;border-radius:0.375em;overflow:hidden;background-color:#dcade6;font-size:1em;}");
  webpage += F("li{float:left;border-radius:0.375em;border-right:0.06em solid #bbb;}last-child {border-right:none;font-size:85%}");
  webpage += F("li a{display: block;border-radius:0.375em;padding:0.44em 0.44em;text-decoration:none;font-size:85%}");
  webpage += F("li a:hover{background-color:#EAE3EA;border-radius:0.375em;font-size:85%}");
  webpage += F("section {font-size:0.88em;}");
  webpage += F("h1{color:white;border-radius:0.5em;font-size:1em;padding:0.2em 0.2em;background:#558ED5;}");
  webpage += F("h2{color:orange;font-size:1.0em;}");
  webpage += F("h3{font-size:0.8em;}");
  webpage += F("table{font-family:arial,sans-serif;font-size:0.9em;border-collapse:collapse;width:85%;}");
  webpage += F("th,td {border:0.06em solid #dddddd;text-align:left;padding:0.3em;border-bottom:0.06em solid #dddddd;}");
  webpage += F("tr:nth-child(odd) {background-color:#eeeeee;}");
  webpage += F(".rcorners_n {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:75%;}");
  webpage += F(".rcorners_m {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:50%;color:white;font-size:75%;}");
  webpage += F(".rcorners_w {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:70%;color:white;font-size:75%;}");
  webpage += F(".column{float:left;width:50%;height:45%;}");
  webpage += F(".row:after{content:'';display:table;clear:both;}");
  webpage += F("*{box-sizing:border-box;}");
  webpage += F("footer{background-color:#eedfff; text-align:center;padding:0.3em 0.3em;border-radius:0.375em;font-size:60%;}");
  webpage += F("button{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:130%;}");
  webpage += F(".buttons {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:80%;}");
  webpage += F(".buttonsm{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:9%; color:white;font-size:70%;}");
  webpage += F(".buttonm {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:70%;}");
  webpage += F(".buttonw {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:40%;color:white;font-size:70%;}");
  webpage += F("a{font-size:75%;}");
  webpage += F("p{font-size:75%;}");
  webpage += F("</style></head><body><h1>ROCKFLY-MINI WEB SERVER ");
  webpage += String(ServerVersion) + "</h1>";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void append_page_footer() {  // Saves repeating many lines of code for HTML page footers
  webpage += F("<ul>");
  webpage += F("<li><a href='/'>Home</a></li>");  // Lower Menu bar command entries
  webpage += F("<li><a href='/download'>Download</a></li>");
  //webpage += F("<li><a href='/upload'>Upload</a></li>");
  webpage += F("<li><a href='/delete'>Delete</a></li>");
  webpage += F("<li><a href='/data'>Data</a></li>");
  webpage += F("<li><a href='/statistics'>Statistics</a></li>");
  webpage += F("<li><a href='/settings'>Settings</a></li>");
  webpage += F("</ul>");
  webpage += "<footer>&copy;appcent</footer>";
  webpage += F("</body></html>");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// All supporting functions from here...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void HomePage() {
  SendHTML_Header();
  webpage += F("<a href='/download'><button>Download</button></a>");
  //webpage += F("<a href='/upload'><button>Upload</button></a>");
  webpage += F("<a href='/delete'><button>Delete</button></a>");
  webpage += F("<a href='/data'><button>Data</button></a>");
  webpage += F("<a href='/statistics'><button>Statistics</button></a>");
  webpage += F("<a href='/settings'><button>Settings</button></a>");
  
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();  // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Download() {      // This gets called twice, the first pass selects the input, the second pass then processes the command line arguments
  if (server.args() > 0) {  // Arguments were received
    if (server.hasArg("download")) DownloadFile(server.arg(0));
  } else {  //SelectInput(String heading1, String command, String arg_calling_name) //SelectInput("Enter filename to download","download","download");
    SendHTML_Header();
    //webpage += F("<h3>Enter filename to download</h3>");
    webpage += F("<FORM action='/download' method='post'>");  // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
    webpage += F("<input type='hidden' name='download' id='download' value=''><br>");
    webpage += F("<type='submit' name='download' value=''><br><br>");
    webpage += F("<a href='/'>[Back]</a><br><br>");
    webpage += F("<table align='center'>");
    webpage += F("<tr><th>Name/Type</th><th>File Size</th><th></th></tr>");
    printDirectoryDownload("/", 0, "Download");
    webpage += F("</table><br><br>");
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void DownloadFile(String filename) {
  if (SPIFFS_present) {
    Serial.println(filename);
    File download = SPIFFS.open("/" + filename, "r");
    if (download) {
      server.sendHeader("Content-Type", "text/text");
      server.sendHeader("Content-Disposition", "attachment; filename=" + filename);
      server.sendHeader("Connection", "close");
      server.streamFile(download, "application/octet-stream");
      download.close();
    } else
      ReportFileNotPresent("download");
  } else
    ReportSPIFFSNotPresent();
}
void printDirectory(const char *dirname, uint8_t levels) {
  File root = SPIFFS.open(dirname);
  if (!root) {
    return;
  }
  if (!root.isDirectory()) {
    return;
  }
  File file = root.openNextFile();
  while (file) {
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if (file.isDirectory()) {
      webpage += "<tr><td>" + String(file.isDirectory() ? "Dir" : "File") + "</th><th>" + String(file.name()) + "</td><td></td></tr>";
      printDirectory(file.name(), levels - 1);
    } else {
      webpage += "<tr><td>" + String(file.name()) + "</td>";
      //webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024) fsize = String(bytes) + " B";
      else if (bytes < (1024 * 1024))
        fsize = String(bytes / 1024.0, 3) + " KB";
      else if (bytes < (1024 * 1024 * 1024))
        fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
      else
        fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";
      webpage += "<td>" + fsize + "</td></tr>";
    }
    file = root.openNextFile();
  }
  file.close();
}
void printDirectoryDownload(const char *dirname, uint8_t levels, const char *buttonName) {
  File root = SPIFFS.open(dirname);
  if (!root) {
    return;
  }
  if (!root.isDirectory()) {
    return;
  }
  File file = root.openNextFile();
  while (file) {
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if (file.isDirectory() ) {
      webpage += "<tr><td>" + String(file.isDirectory() ? "Dir" : "File") + "</th><th>" + String(file.name()) + "</td><td></td></tr>";
      printDirectory(file.name(), levels - 1);
    } else {
      if(file.size()>0){
      webpage += "<tr><td>" + String(file.name()) + "</td>";  //
      //webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024) fsize = String(bytes) + " B";
      else if (bytes < (1024 * 1024))
        fsize = String(bytes / 1024.0, 3) + " KB";
      else if (bytes < (1024 * 1024 * 1024))
        fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
      else
        fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";
      
      webpage += "<td>" + fsize + "</td><td><center><input type='submit' onclick=\"change('" + String(file.name()) + "');\" value='" + String(buttonName) + "'></center></td></tr>";
        
      }
    }
    file = root.openNextFile();
  }
  file.close();
}

void Data() {
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<h3>System Information</h3>";
  webpage += "<h4>MS5611</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Temp</th><th>Altitude</th><th>Velocity</th><tr>";
  webpage += "<tr><td>" + String(flightdata.Ms5611_data[0]) + "</td><td>" + String(flightdata.Ms5611_data[2]) + "</td><td>" + String(flightdata.Ms5611_data[3]) + "</td></tr>";
  webpage += "</table>"; 
  webpage += "<h3> </h3>";  //"<tr><th>Time</th><th>Date</th><tr>";
  webpage += "<tr><th> Date: </th><th>";
  webpage += "<tr><td>" + String(rtc.getDay()) + "/" + String(rtc.getMonth() + 1) + "/" + String(rtc.getYear()) + "</td></tr> ";
  webpage += "</table>";  
  webpage += "<h3> </h3>";
  webpage += "<tr><th> Time: </th><th>";
  webpage += "<tr><td>" + String(rtc.getHour()) + ":" + String(rtc.getMinute()) + ":" + String(rtc.getSecond()) + "</td><tr>";
  webpage += "</table>";
  webpage += "<h4>PYRO INFORMATION</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Batt</th><th>Pyro 1</th><th>Pyro 2</th></tr>";
  webpage += "<tr><td>" + String(flightdata.battery) + "</td><td>" + String(pyroStatArr[1]) + "</td><td>" + String(pyroStatArr[0]) + "</td></tr>";
  webpage += "</table> ";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}


void Statistics() {
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<h2> Statistics Data </h2>";

  switch(EEPROM.read(44))
    {
     case 0:
        
        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Max Altitude: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(4)) + String(" ") +  String("m") +"</td><tr>";
        webpage += "</table>";
        
        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Max Speed: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(8)) + String(" ") + String("km/h") +"</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Max Accel: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(12)) + String(" ") + String("g") +"</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Apogee Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(16)) + String(" ") + String("s") +"</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Apogee Fall Speed: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(20)) + String(" ") + String("km/h") + "</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Main Fall Speed: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(24)) + String(" ") + String("km/h") + "</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Descend Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(28)) + String(" ") + String("s") +"</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Average Rise Accel: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(32)) + String(" ") + String("g") + "</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Average Fall Accel: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(36)) + String(" ") + String("g") +"</td><tr>";
        webpage += "</table>";

        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Pyro Activation Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(40)) + String(" ") + String("ms") + "</td><tr>";
        webpage += "</table>";
        
        webpage += "<table class='center'>";
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Flight Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(51)) + String(" ") + String("s") + "</td><tr>";
        webpage += "</table>";
    
        append_page_footer();
        SendHTML_Content();
        SendHTML_Stop();                  
     break;
      
     case 1:
        webpage += "<h3> </h3>";
        webpage += "<tr><th> Max Altitude: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(4)) + String(" ") + String("ft") +"</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Max Speed: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(8)) + String(" ") + String("mph") +"</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Max Accel: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(12)) + String(" ") + String("g") +"</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Apogee Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(16)) + String(" ") + String("s") +"</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Apogee Fall Speed: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(20)) + String(" ") + String("mph") + "</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Main Fall Speed: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(24)) + String(" ") + String("mph") + "</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Descend Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(28)) + String(" ") + String("s") +"</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Average Rise Accel: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(32)) + String(" ") + String("g") + "</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Average Fall Accel: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(36)) + String(" ") + String("g") +"</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Pyro Activation Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(40)) + String(" ") + String("ms") + "</td><tr>";
        webpage += "</table>";

        webpage += "<h3> </h3>";
        webpage += "<tr><th> Flight Time: </th><th>";
        webpage += "<tr><td>" +  String(EEPROM.readFloat(51)) + String(" ") + String("s") + "</td><tr>";
        webpage += "</table>";
    
        append_page_footer();
        SendHTML_Content();
        SendHTML_Stop();
        
     break;        
    }  
}

void Settings() {
  if (server.args() > 0) {  // Arguments were received
    if (server.hasArg("settings")|| server.hasArg("imperial")|| server.hasArg("metric")) {
      SendHTML_Header();
      webpage += "<h3>Data has been save</h3>";
      webpage += F("<a href='/settings'>[Back]</a><br><br>");
      Serial.print("server.arg(0)=");
      
      mainPar = server.arg(0).toFloat();
      if(mainPar>0){ 
      mainParChange = mainPar;  //Güncel main paraşüt değeri kodda 5 metre iken kart resetlenip tekrar açıldığında eppromda kalan sayıyı almaması için.
      EEPROM.writeFloat(0, mainPar);      
      EEPROM.commit();
      Serial.println(server.arg(0)); }
                 
      int choosen = server.arg(1).toInt();
      if(choosen>0){
      if(choosen==2) measurementSystem=0;
      else if(choosen==1) measurementSystem=1; 
      
      EEPROM.write(44, measurementSystem);      
      EEPROM.commit();}
      
      Serial.print("server.arg(1)=");
      Serial.println(server.arg(1));
      
      append_page_footer();
      SendHTML_Content();
      SendHTML_Stop();
    }

  } else {
    SendHTML_Header();
    //webpage += "<meta http-equiv='refresh' content='5'>";
    webpage += "<form action='/settings' method='post'><br><br>";
    webpage += "<label for='fmainPar'>Main Parachute:</label>";
    webpage += "<input type='text' name='settings' id='settings' value='" + String(mainParChange) + "'><br><br>";
    webpage += "<table class='center'>";
    webpage += "<label for='metric'>METRIC</label>";
    webpage += "<input type='checkbox' name='metric' id='metric' value='2'><br><br>";
    webpage += "<table class='center'>";
    webpage += "<label for='imperial'>IMPERIAL</label>";
    webpage += "<input type='checkbox' name='imperial' id='imperial' value='1'><br><br><br><br>";
   
    webpage += "<button type='submit'>Submit</button><br><br>";
    webpage += "</form>";
    
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Delete() {
  if (server.args() > 0) {  // Arguments were received
    if (server.hasArg("delete")) SPIFFS_file_delete(server.arg(0));
  } else {  //SelectInput(String heading1, String command, String arg_calling_name) //SelectInput("Enter filename to download","download","download");
    SendHTML_Header();
    //webpage += F("<h3>Enter filename to download</h3>");
    webpage += F("<FORM action='/delete' method='post' onsubmit=\"return confirm('Do you really want to delete the file?');\"> ");  // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
    webpage += F("<input type='hidden' name='delete' id='download' value=''><br>");
    webpage += F("<type='submit' name='delete' value=''><br><br>");
    webpage += F("<a href='/'>[Back]</a><br><br>");
    webpage += F("<table align='center'>");
    webpage += F("<tr><th>Name/Type</th><th>File Size</th><th></th></tr>");
    printDirectoryDownload("/", 0, "Delete");
    webpage += F("</table><br><br>");
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SPIFFS_file_delete(String filename) {  // Delete the file
  if (SPIFFS_present) {
    SendHTML_Header();
    //File dataFile = SPIFFS.open("/"+filename, "r"); // Now read data from SPIFFS Card
    //if (dataFile)
    //{
    if (SPIFFS.remove("/" + filename)) {
      Serial.println(F("File deleted successfully"));
      webpage += "<h3>File '" + filename + "' has been erased</h3>";
      webpage += F("<a href='/delete'>[Back]</a><br><br>");
    } else {
      webpage += F("<h3>File was not deleted - error</h3>");
      webpage += F("<a href='delete'>[Back]</a><br><br>");
    }
    //} else ReportFileNotPresent("delete");
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  } else
    ReportSPIFFSNotPresent();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Header() {
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", "");  // Empty content inhibits Content-length header so we have to close the socket ourselves.
  append_page_header();
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Content() {
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Stop() {
  server.sendContent("");
  server.client().stop();  // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SelectInput(String heading1, String command, String arg_calling_name) {
  SendHTML_Header();
  webpage += F("<h3>");
  webpage += heading1 + "</h3>";
  webpage += F("<FORM action='/");
  webpage += command + "' method='post'>";  // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='text' name='");
  webpage += arg_calling_name;
  webpage += F("' value=''><br>");
  webpage += F("<type='submit' name='");
  webpage += arg_calling_name;
  webpage += F("' value=''><br><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  webpage += F("<table align='center'>");
  webpage += F("<tr><th>Name/Type</th><th>File Size</th></tr>");
  printDirectory("/", 0);
  webpage += F("</table><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportSPIFFSNotPresent() {
  SendHTML_Header();
  webpage += F("<h3>No SPIFFS Card present</h3>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportFileNotPresent(String target) {
  SendHTML_Header();
  webpage += F("<h3>File does not exist</h3>");
  webpage += F("<a href='/");
  webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportCouldNotCreateFile(String target) {
  SendHTML_Header();
  webpage += F("<h3>Could Not Create Uploaded File (write-protected?)</h3>");
  webpage += F("<a href='/");
  webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
String file_size(int bytes) {
  String fsize = "";
  if (bytes < 1024) fsize = String(bytes) + " B";
  else if (bytes < (1024 * 1024))
    fsize = String(bytes / 1024.0, 3) + " KB";
  else if (bytes < (1024 * 1024 * 1024))
    fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
  else
    fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";
  return fsize;
}
